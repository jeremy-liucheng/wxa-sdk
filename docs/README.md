---
home: true
heroImage: https://cdn.11h5.com/static/image/logo_x.png
actionText: 快速上手 →
actionLink: /zh/guide/
actions:
  - text: 快速上手
    link: /platform/wx/wxaMainFrontend.html
    type: primary
footer: MIT Licensed | Copyright © 2018-present CodyLiu
---