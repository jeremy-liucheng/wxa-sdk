# 接入准备

::: danger
sdk 调用时机（请严格遵守）：请在小游戏头包解压完，小游戏侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.
:::

联系运营确定使用的域名，如运营提供的为`xxxx.com`，那么此项目 sdk 内用到的所有协议域名都是`xxxx.com`（下文中所有`xxxx.com`都是这个域名），下面将正式开始~

soul 小游戏后台配置

设置->开发管理->服务器域名->请求 ip： 172.81.246.125

设置->开发管理->服务器域名->服务器相对地址： 'https://游戏资源相对地址/'

设置->基础设置->功能设置->启动页： /xxxx/index.html

### [下载 sdk](/sdk/soul/sdk_soul_0.0.1.min.js.zip)

### 初始化 sdk

```html
<html>
  <head></head>
  <body>
    <script src="soul_sdk本地路径"></script>
    <script>
      const sdk = new sdk_soul({
          // 必填字段，传入当前游戏ID
          gameid: gameid,
          // 必填字段，当前小游戏的appid
          appid: appid,
          // 选填字段，联系运营提供
          domain?: 'xxx.com',
      })
    </script>
  </body>
</html>
```

### 登录

```js
// uid默认为0，token为空
// 如果登录成功，将可获得用户的uid，token信息
sdk
  .login()
  .then(({ uid, token }) => {
    // 登录成功
  })
  .catch((res) => {
    // 登录失败
  });
```

### 更新用户信息

静默授权，无法获取用户的头像、昵称等敏感信息

如果项目需要获取用户的个人信息，需要调用此方法，进行信息同步

```js

const userInfo = await sdk.updateUserInfo()

UserInfo {
    /** 用户昵称 */
    nickName: string;
    /** 用户头像图片的 URL。*/
    avatarUrl: string;
    /** 用户性别 */
    gender: 0 | 1 | 2;
    /** 用户所在城市 */
    area: string;
}

```

### 支付准备

<strong class="red">此功能在游戏上线前必须接入</strong>

商户后台配置商品，如下：

<img :src="$withBase('/images/soul/shop.png')" alt="Image from dependency">

商户后台私有参数

<img :src="$withBase('/images/soul/soul_config.png')" alt="Image from dependency">

### 配置人民币兑换游戏币比例

游戏货币充值比例：固定填 7
<img :src="$withBase('/images/soul/soul_money_onfig.png')" alt="Image from dependency">

### 支付

```js
// ios和安卓端通用
sdk.pay({
    /** 商户后台配置商品后，生成的商品ID */
    product_id: '商品ID',
    /**
     * 代表购买商品的数量，默认值为1。
     * 批量购买商品时使用，最大不要超过1000。
     * 如果不为空，则会在通知发货接口中返回此参数。
     */
    product_count: number,
    /** 当前的区服  */
    serverid: '1',
    /**
     * 商户订单号，最多64个字节。
     * 如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。
     */
    txid?: string,
    /** 商户自定义参数，最多128个字节。
     * 如果不为空，则会在通知发货接口中返回给商户，请对该数据进行url转义，否则可能会丢失数据，
     * 可以参考js的encodeURI或者php中的urlencode进行转义。 */
    userdata?: string,
}).then((res) => {
    // 支付成功 res: { error: 0, errmsg: '操作成功', trans_id }
}).catch((res) => {
    // 支付失败 res: { code: xxxx }
})

```

### 有效创角上报

双方约定当用户达到有效创角的条件，调用下面的方法进行上报

```js
sdk.effectiveStat();
```

### 判断前置游戏是否开启

```js
const isOpen = await sdk.isOpenPreGame();
// isOpen: true 开启 ，false 关闭
```

### 用户完成前置小游戏

<strong class="red">仅在前置游戏开启情况下调用</strong>

```js
sdk.achievePreGame();
```
