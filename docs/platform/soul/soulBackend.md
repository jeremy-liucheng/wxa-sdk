# 服务端接入

## 必接

### 一、聊天上报

#### 接口：

- 涉及游戏对接密钥，请务必通过后台接入
- 地址: https://chat-monitor.{项目域名}.com/msg/?cmd=record
- 请求方式: [POST] application/json
- 用户侧非必填参数请根据运营需求决定是否传入

#### 参数:

| 字段名称     | 字段类型 | 是否必填 | 字段说明                                                                            |
| :----------- | :------- | :------- | :---------------------------------------------------------------------------------- |
| gameid       | Int      | 是       | 商户后台分配的游戏 id                                                               |
| openid       | String   | 是       | 平台授权、鉴权返回的用户 id，例如：ebe28b44645761741b32fe1fbbcf9ed1 或者 3006658266 |
| userplt      | String   | -        | 用户来源渠道 渠道名缩写，爱微游默认不填                                             |
| serverid     | String   | 是       | 区服 id，请尽量与游戏前台保持一直                                                   |
| accountid    | String   | 是       | 玩家游戏侧账号                                                                      |
| roleid       | String   | 是       | 游戏内角色 id                                                                       |
| rolename     | String   | 是       | 游戏内角色名                                                                        |
| rolelv       | String   | -        | 角色等级                                                                            |
| rolepwr      | String   | -        | 角色战力                                                                            |
| channel      | Int      | 是       | 聊天频道，1:私聊，2:房间，3:区服(单服)，4:世界(全服)，5:帮会同盟，6:队伍, 7: 其他   |
| channelid    | String   | -        | 聊天频道号                                                                          |
| recvopenid   | String   | -        | 聊对象 用户平台 id                                                                  |
| recvplt      | String   | -        | 私聊对象 来源渠道 渠道名缩写，爱微游默认不填                                        |
| recvroleid   | String   | -        | 私聊对象 角色 id                                                                    |
| recvrolename | String   | -        | 私聊对象 角色名                                                                     |
| msgcont      | String   | 是       | 消息内容                                                                            |
| time         | Int      | 是       | 消息发送时间戳（秒），例如：1640150557                                              |
| sign         | String   | 是       | 参照文档签名规则,例：ead66cdf77292ef986be0278f91340366c41eaf3                       |
| ext          | String   | -        | 扩展参数， 最大长度 255，例如：key1%3Dval1%26key2%3Dval2                            |

##### 返回：
```js
{
    "error": 0,// 错误码，0为成功，其他为异常
    "errmsg": ""// 错误提示
}
```

### 二、提供禁言接口

#### 接口：
* 游戏方接收禁言请求的接口
* 配置到商户后台额外参数
* 参数key：**forbidChatUrl**
* 参数value：**游戏方禁言接口地址**
* 请求方式: [POST] application/json
#### 参数:

| 字段名称 | 字段类型 | 是否必填 | 字段说明 |
| :-- | :-- | :-- | :-- |
| gameid | Int | 是 | 商户后台分配的游戏id |
| openid | String | 是 | 平台用户id，例如：ebe28b44645761741b32fe1fbbcf9ed1 或者 3006658266 |
| accountid | String | 是 | 玩家游戏侧账号 |
| roleid | String | 是 | 游戏内角色id |
| forbidDays | Int | 是 | 禁言天数，1 ~ 30 禁言时间，-1 永久 |
| reason | String | 是 | 禁言理由 |
| time | Int | 是 | 服务器当前时间戳（秒），例如：1640150557 |
| sign | String | 是 | 参照文档签名规则,例：ead66cdf77292ef986be0278f91340366c41eaf3 |
| ext | String | - | 若聊天记录有传此参数，则原样返回 |

#### 返回：
```js
{
    "error": 0,// 错误码，0为成功，其他为异常
    "errmsg": ""// 错误提示
}
```

### 三、提供解除禁言接口

#### 接口：
* 游戏方接收禁言请求的接口
* 配置到商户后台额外参数
* 参数key：**freeChatUrl**
* 参数value：**游戏方解除禁言接口地址**
* 请求方式: [POST] application/json
#### 参数:

| 字段名称 | 字段类型 | 是否必填 | 字段说明 |
| :-- | :-- | :-- | :-- |
| gameid | Int | 是 | 商户后台分配的游戏id |
| openid | String | 是 | 平台用户id，例如：ebe28b44645761741b32fe1fbbcf9ed1 或者 3006658266 |
| accountid | String | 是 | 玩家游戏侧账号 |
| roleid | String | 是 | 游戏内角色id |
| time | Int | 是 | 服务器当前时间戳（秒），例如：1640150557 |
| sign | String | 是 | 参照文档签名规则,例：ead66cdf77292ef986be0278f91340366c41eaf3 |
| ext | String | - | 若封禁的聊天记录有传此参数，则原样返回 |

##### 返回：
```js
{
    "error": 0,// 错误码，0为成功，其他为异常
    "errmsg": ""// 错误提示
}
```

### 四、提供角色查询接口

#### 接口：
* 游戏方接收禁言请求的接口
* 配置到商户后台额外参数
* 参数key：**chatRoleUrl**
* 参数value：**游戏方角色查询接口地址**
* 请求方式: [POST] application/json
#### 参数:

| 字段名称 | 字段类型 | 是否必填 | 字段说明 |
| :-- | :-- | :-- | :-- |
| gameid | Int | 是 | 商户后台分配的游戏id |
| serverid | String | 是 | 区服id，请尽量与游戏前台保持一直 |
| openid | String | 是 | 平台用户id，例如：ebe28b44645761741b32fe1fbbcf9ed1 或者 3006658266 |
| accountid | String | 是 | 玩家游戏侧账号 |
| roleid | String | 是 | 游戏内角色id |
| time | Int | 是 | 服务器当前时间戳（秒），例如：1640150557 |
| sign | String | 是 | 参照文档签名规则,例：ead66cdf77292ef986be0278f91340366c41eaf3 |
| ext | String | - | 若封禁的聊天记录有传此参数，则原样返回 |

##### 返回：
```js
{
    "error": 0,// 错误码，0为成功，其他为异常
    "errmsg": "",//错误信息
    "role": {
        "rolename": "xxx",// 角色名
        "rolelv": 119,// 角色等级
        "rolepwr": 100000000,// 角色战力
        "viplv": 15,// 游戏内vip等级
        "alliance": "xxx",// 公会名称
        "paymoney": 1000,// 角色充值金额，单位元
    }
}
```

## 选接

### 鉴权

<strong class="red">本接口由服务端调用</strong>

- 内网地址 `http://10.0.34.7/wxlogin-svr/wxlogin`
- 外网地址 `https://wxlogin.xxxx.com/wxlogin`

请求方式：GET

请求参数

| 字段名称 | 字段类型 | 是否必填 | 字段说明                |
| -------- | -------- | -------- | ----------------------- |
| cmd      | String   | 是       | 固定填写 checkUserToken |
| token    | String   | 是       | 用户登录态              |

返回值

| 字段名称   | 字段类型 | 字段说明                                                                                 |
| ---------- | -------- | ---------------------------------------------------------------------------------------- |
| uid        | Number   | 用户的 uid                                                                               |
| nickname   | String   | 用户的昵称                                                                               |
| headimgurl | String   | 用户的头像 url                                                                           |
| sex        | Number   | 0 表示未知<br>1 表示男<br>2 表示女                                                       |
| province   | String   | 省市                                                                                     |
| city       | String   | 市                                                                                       |
| authFlag   | Number   | 1 表示静默授权用户(玩家未授权获取昵称头像等信息)<br>0 表示显性授权用户(可以拿到头像昵称) |
| focus      | Number   | 是否关注公众号<br>0 代表未关注<br>1 代表已关注                                           |

### 获取令牌

<strong class="red">本接口由服务端调用，游戏侧如果要获取玩家的 access_token，必须通过此接口获取，非抖音文档提供的接口</strong>

- 地址：`https://api.xxxx.com/mpcommon`

请求方式：GET

请求参数

|   参数    |  类型  |             说明              | 是否必须 |
| :-------: | :----: | :---------------------------: | :------: |
|    cmd    | String | 固定填写 byteDanceAccessToken |    是    |
|  gameid   | Number |            游戏 id            |    是    |
|   appid   | String |        小游戏的 appid         |    是    |
| timestamp | Number |     当前时间戳（单位秒）      |    是    |
|   nonce   | Number |           随机整数            |    是    |
| signature | String |       见附录签名算法 1        |    是    |

返回值

<strong class="red">获取 access_token 之后，请服务端将其缓存 1 分钟左右(缓存时间不要超过 5 分钟)，以减少不必要的开销。</strong>

```js
{
  access_token: 'access_token'; //成功则返回下列数据，如果发生错误，返回error
}
```

### 创角上报

<strong class="red">本接口由服务端调用，此功能在游戏上线前必须接入</strong>

- 地址：`https://platform.xxxx.com/stat/api/`

请求方式：GET

请求参数

| 字段名称   | 字段类型 | 是否必填 | 字段说明                   |
| ---------- | -------- | -------- | -------------------------- |
| cmd        | String   | 是       | 固定填写 getCreateRoleInfo |
| openId     | String   | 是       | 用户的 UID，通过登录获取   |
| time       | Number   | 是       | 创角时间戳（单位秒）       |
| gameId     | Number   | 是       | 游戏 id                    |
| serverId   | Number   | 是       | 服务器 ID                  |
| playerName | String   | 是       | 角色名(中文请 urlencode)   |
| sign       | String   | 是       | 见附录签名算法 2           |

返回值

```js
{
  error: 0; //成功
  1; //失败
  4; //缺少参数
  5; //签名错误
  6; //非法数据
}
```

## 附录

### 签名规则

- 将**游戏 id、平台用户 id、时间戳、游戏密钥**，通过 & 符连接获取到加密字符串
- 将加密字符串通过 md5 加密算法获取到 32 位 小写字符串 即为请求签名

```js
let gameid = 961; // 商户平台分配的游戏id
let openid = '3006658266'; // 平台用户uid
let time = 1657079977; // 当前时间戳
let key = 'rKgNvSwrEdK0egNXjVMNdZONbyoYMp08'; // 商户后台游戏密钥
let signstr = [gameid, openid, time, key].join('&'); // 签名字符串
console.log('签名字符串:', signstr);
let sign = md5(signstr);
console.log('sign:', sign);
```

### 签名算法 1

- 开发者和服务器之间约定支付秘钥（商户后台添加）

- 开发者将以下三个参数填写在请求的服务器 url 中

|   参数    |   描述   |
| :-------: | :------: |
| timestamp |  时间戳  |
|   nonce   | 随机整数 |
| signature | 加密签名 |

- signature 加密流程如下：

  - 将支付密钥、timestamp、nonce 三个参数进行**`字典序`**排序
  - 将三个参数字符串拼接成一个字符串进行 sha1 加密即可获得 signature

- 服务端根据开发者传过来的 timestamp 和 nonce 参数以及约定好的支付密钥，通过第 3 步加密流程算法同样计算出一个 signature,与开发者传过来的参数进行比对，相同则验证通过

> JS 代码示例

```js
//token为约定的秘钥，timestamp为当前时间戳，nonce为随机整数
var calSign = function (密钥, timestamp, nonce) {
  let arr = [密钥, timestamp, nonce];
  arr.sort();

  let str = arr.join('');
  let sign = crypto.createHash('sha1').update(str).digest('hex');

  return sign;
};
```

> PHP 代码示例

```php

<?php
$nonce = 89143569; //TODO: 自己随机一个数
$timestamp = 1551782559; //TODO: 当前时间
$token = ''; //TODO:秘钥

function calSign($token, $timestamp, $nonce) {
    $arr =  array($nonce, $timestamp, $token);
    asort($arr,SORT_STRING);
    $str = implode($arr,'');
    return sha1($str);
}

//计算签名
$signature = calSign($token, $timestamp, $nonce);

//拼接url
$url = "https://api.xxxx.com/mpcommon/?timestamp=$timestamp&gameid=533&appid=wxbf8bf321f0a7ccf3&nonce=$nonce&cmd=cmd=externalGetGameAccessToken&signature=$signature";

echo "$url";

```

### 签名算法 2

- 参数名按字典排序
- 如果 playerName 是中文，需要 urlencode
- secret 运营提供
- 在实际计算中去掉[]

```js
sign = md5(
  'gameId=[xxx]openId=[xxx]playerName=[xxx]secret=[xxx]serverId=[xxx]time=[xxx]'
);
```

### 签名算法 3

在请求 URL 参数列表中，除去 sign 参数外，其他需要使用到的参数皆是要签名的参数。对于如下的参数数组：

```js

parameters = {
    "uid=12345678",
    "rmb=1",
    "reqid=20152392834940",
    "trans_id=123456",
    "product_id=6000",
      "notify_id=f2c90bc120cdef27c8cc4d1bdf8341a74625588c6edcd9c",
    "nick=",
};

```

除去为空的参数，对数组里的每一个值从 a 到 z 的顺序排序，若遇到相同首字母，则看第二个字母，以此类推。
排序完成之后，再把所有数组值以“&”字符连接起来，如面这个参数数组生成请求如下 :

```js
//这串字符串便是待签名字符串。
`notify_id=f2c90bc120cdef27c8cc4d1bdf8341a74625588c6edcd9c&product_id=6000&reqid=20152392834940&rmb=1&trans_id=123456&uid=12345678`;
```

当拿到请求时的待签名字符串后，需要把“key=私钥”这个参数拼接到待签名字符串后面，形成新的字符串，示例如下：

```js
`notify_id=f2c90bc120cdef27c8cc4d1bdf8341a74625588c6edcd9c&product_id=6000&reqid=20152392834940&rmb=1&trans_id=123456&uid=12345678&key=xxxxxxxx`;
```

利用 MD5 的签名函数对这个新的字符串进行签名运算，再转成大写字母，从而得到 32 位签名结果字符串（该字符串赋值于参数 sign）。
假如 key=yduLY3ovg4NwTHMMemGg1vO6VHuYBcYD，则签名等于

```js
strtoupper(
  md5(
    'notify_id=f2c90bc120cdef27c8cc4d1bdf8341a74625588c6edcd9c&product_id=6000&reqid=20152392834940&rmb=1&trans_id=123456&uid=12345678&key=yduLY3ovg4NwTHMMemGg1vO6VHuYBcYD'
  )
);
```

结果是

```js
`251161041BD42E46379C22A87819C740`;
```

通知返回时验证签名

当获得到通知返回时的待签名字符串后，同理，需要把私钥直接拼接到待签名字符串后面，形成新的字符串，利用 MD5 的签名函数对这个新的字符串进行签名运算，从而得到 32 位签名结果字符串。此时这个新的字符串需要与接口通知返回参数中的参数 sign 的值进行验证是否相等，来判断签名是否验证通过。



