# 初始化

#### 小游戏后台开发设置

请将以下域名，加入到小游戏 **request** 白名单

> `https://wxlogin.kxtoo.com;https://adstat.kxtoo.com`

请将以下域名，加入到小游戏 **download** 白名单

> `https://act.kxtoo.com`

### [下载 sdk](/wxa-sdk/sdk/zhongzhuan/sdk-v2.zip?v=3)

#### 引入 sdk

```js
import Sdk from "本地sdk路径";
```

#### 获取用户来源

gameid 游戏已有的 gameid

::: danger
调用时机（请严格遵守）：请在小游戏头包解压完，微信侧做出首屏渲染之前即刻调用 sdk.userSource，即所有游戏侧执行代码的最前部。
:::

```js
sdk.userSource(gameid);
```

##### 功能说明

::: danger
判断用户是否从广告进入展示不同内容
:::

##### 示例代码

```js
const res = await sdk.userSource(1189);
if (res.error === 0) {
  // 用户为广告用户 - 展示中转落地页
} else {
  // 普通用户 - 展示普通小游戏
}
```

[从广告进入的用户展示落地页>](#落地页示例)

##### 参数说明

Object res

| 属性        | 类型   | 默认值 | 说明                                                 |
| :---------- | :----- | :----- | :--------------------------------------------------- |
| error       | number |        | 0: [展示中转落地页>](#落地页示例)，1: 展示普通小游戏 |
| picImg      | string |        | 落地页素材地址                                       |
| headerImage | string |        | 头部图片                                             |

#### 点击复制链接

```js
sdk.copy(type);
```

#### 参数说明

Number type

| 属性 | 类型   | 默认值 | 说明                                    |
| :--- | :----- | :----- | :-------------------------------------- |
| type | number |        | 2 点击落地页,3 点击弹窗中的复制网址按钮 |

##### 功能说明

复制链接

::: danger
如果出现复制失败 1026 需在微信后台更新隐私设置 设置->基本设置->服务内容声明->用户隐私保护指引
:::

#### 落地页示例

::: danger
落地页高度可能不定需要游戏适配滚动

[提示复制弹窗素材下载>](http://platform-doc.ttwmz.com/docs/mains/platform/imgs/material.zip)

[头部图示例>](https://act.11h5.com/gc/images/2023/1121/20231121103404481.jpg)

[大图示例](https://act.11h5.com/gc/images/2023/1121/20231121103517921.jpg)
:::

<img :src="$withBase('/images/zz/landPage.png')" alt="Image from dependency">

点击大图和进入游戏调用 sdk.copy() 之后弹出提示复制

<img :src="$withBase('/images/zz/tips.png')" alt="Image from dependency">

弹出的复制提示图片中的'点击复制网址'继续调用 sdk.copy()，下方关闭按钮可以关闭弹窗

::: danger
头部内容需固定在页面顶部，需要一直可见
:::

<img :src="$withBase('/images/zz/header.png')" alt="Image from dependency">

#### 如何调试

在微信开发工具中添加启动参数运行小游戏 模拟广告进入参数

```js
chid=4706&subchid=TEST12322222222&gameid=1185
// 联系cody提供
```

chid 广告带来的参数

subchid 广告带来的参数

gameid 区别于小游戏自己的 id 这里是广告带来的 gameid
