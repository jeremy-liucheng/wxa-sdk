# 前端接入准备

::: danger
sdk 调用时机（请严格遵守）：请在小游戏头包解压完，微信侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.
:::

联系运营确定使用的域名，如运营提供的为`xxxx.com`，那么此项目 sdk 内用到的所有协议域名都是`xxxx.com`（下文中所有`xxxx.com`都是这个域名），下面将正式开始~

请将以下域名，加入到小游戏 **request** 白名单

> `https://adapi.xxxx.com,https://act.xxxx.com,https://newintegral-wall.xxxx.com,https://adstat.xxxx.com,https://wxlogin.xxxx.com,https://platform.xxxx.com,https://api.xxxx.com,https://login.xxxx.com,https://online.xxxx.com`

请将以下域名，加入到小游戏 **downloadFile** 白名单

> `https://api.xxxx.com,https://act.xxxx.com`

### [下载 sdk](/wxa-sdk/sdk/wxa/sdk_wxa.2.5.4.zip?v=1)

## 必接

### 初始化 sdk

```js

import Sdk from '本地sdk路径';

const sdk = new Sdk({
    // 必填字段，传入当前游戏ID
    gameid: gameid,
    // 必填字段，当前小游戏的appid
    appid: appid,
    // 选填字段，联系运营提供
    domain?: 'xxx.com',
    // 选填字段，打开积分墙广告测试环境
    wallTest?: true
})

```

### 登录

sdk.login().then(res)

##### 功能说明

获取平台 uid token 等 token 信息

##### then 回调参数

Object res

| 属性  | 类型   | 说明         |
| ----- | ------ | ------------ |
| uid   | number | 平台用户 uid |
| token | string | 平台 token   |

##### 示例代码

```js
// uid默认为0，token为空（token有效期为12个小时）
// 如果登录成功，将可获得用户的uid，token信息
// token会缓存在storge里面 下次登陆验证token有没有过期 （注意：删除uid token如果还在有效期会返回老的uid）
// 默认为h5互通版本
sdk
  .login()
  .then(({ uid, token }) => {
    // 登录成功
  })
  .catch((res) => {
    // 登录失败
  });
```

### 更新用户信息

sdk.updateUserInfo(Object object).then(res)

##### 功能说明

静默授权，无法获取用户的头像、昵称等敏感信息

如果项目需要获取用户的个人信息，需要调用此方法，进行信息同步

该方法接受一个选填的对象参数，对象里面的 lang 参数可设置返回结果中的文本语言类型，可选值 'en' | 'zh_CN' | 'zh_TW'，默认为 en

##### 参数

Object object

| 属性 | 类型   | 默认值 | 说明                                                                   |
| ---- | ------ | ------ | ---------------------------------------------------------------------- |
| lang | string | en     | 返回结果中的文本语言类型。 可选值 'en' ， 'zh_CN' ，'zh_TW'，默认为 en |

##### 返回参数

Object res

| 属性      | 类型   | 说明                                                                                                                                                                                                                                            |
| --------- | ------ | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| nickName  | string | 用户头像图片的 URL。URL 最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，0 代表 640x640 的正方形头像，46 表示 46x46 的正方形头像，剩余数值以此类推。默认 132），用户没有头像时该项为空。若用户更换头像，原有头像 URL 将失效。 ｜ |
| avatarUrl | string | 用户没有头像时该项为空。若用户更换头像，原有头像 URL 将失效                                                                                                                                                                                     |
| gender    | number | 用户性别                                                                                                                                                                                                                                        |
| country   | string | 用户所在国家                                                                                                                                                                                                                                    |
| province  | string | 用户所在省份                                                                                                                                                                                                                                    |
| city      | string | 用户所在城市                                                                                                                                                                                                                                    |
| language  | string | 显示 country，province，city 所用的语言                                                                                                                                                                                                         |

##### 示例代码

```js
const userInfo = await sdk.updateUserInfo({ lang: "en" });
```

### 有效创角上报

sdk.effectiveStat()

##### 功能说明

::: danger
重度游戏玩家在线 10 分钟调用，轻度游戏玩家在线 5 分钟调用，具体时间有异议请联系运营同学确定
:::

##### 示例代码

```js
sdk.effectiveStat();
```

### 联系客服

wx.openCustomerServiceConversation({})

##### 功能说明

::: danger
此功能在游戏上线前必须接入，客服入口 icon 要放在显眼位置！！
:::

微信小游戏基础库 2.0.3 开始支持，低版本需做兼容处理。
进入客服会话。要求在用户发生过至少一次 touch 事件后才能调用。

```js
wx.openCustomerServiceConversation({});
```

::: danger
备注：建议在加载也增加【客服】按钮，方便游戏加载失败，玩家可联系客服
:::

## 有内购功能游戏必接入

### 苹果支付

#### 支付准备

::: danger
此功能游戏内有内购功能，上线前必须接入
:::

商户后台配置商品，如下：

<img :src="$withBase('/images/wxa/shop.png')" alt="Image from dependency">

#### 支付检测

sdk.checkPay().then(Object res)

##### 功能说明

苹果设备渲染支付面板之前，需要调用此方法，根据结果来决定是否渲染支付面板&充值

##### 返回参数

Object res

| 属性   | 类型                                                                                       | 说明                                                                                                            |
| ------ | ------------------------------------------------------------------------------------------ | --------------------------------------------------------------------------------------------------------------- |
| result | number                                                                                     | 0: 个人主体的小游戏禁止充值，公司主体可采用官方支付，需与运营沟通，根据情况决定 1: 可以充值，可采用非官方的方式 |
| config | <details><summary>Object</summary>例如 lv: 10, // 代表需要等级达到 10 级才能充值</details> | 充值相关的配置                                                                                                  |

##### 示例代码

```js
const { result, config } = await sdk.checkPay();
```

#### 支付

sdk.pay(Object object).then(Object res)

::: danger
需要准备[图 1](#图1) 所示 cardImageUrl thumb_url 两张符合游戏风格的图片素材
:::

##### 参数 payObject

Object object

| 属性                | 类型   | 必填 | 默认值       | 说明                                                                                                                                                                                  |
| ------------------- | ------ | ---- | ------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| gname               | string | 否   | 游戏名称     | 游戏名称                                                                                                                                                                              |
| product_id          | number | 是   |              | 商品 id                                                                                                                                                                               |
| product_count       | number | 否   | 1            | 批量购买商品时使用，最大不要超过 1000 如果不为空，则会在通知发货接口中返回此参数                                                                                                      |
| serverid            | string | 是   |              | 区服 id                                                                                                                                                                               |
| env                 | string | 否   | 0            | env = 1 测试环境 env = 0 正式环境                                                                                                                                                     |
| cardImageUrl        | string | 是   |              | 玩家推送给客服卡片上的图片 （字段解释见[图 1](#图1)）                                                                                                                                 |
| title               | string | 否   | 点我进行充值 | 客服推送给玩家消息上的标题 （字段解释见[图 1](#图1)）                                                                                                                                 |
| offerId             | string | 是   |              | 米大师应用 ID                                                                                                                                                                         |
| midasPaymentVersion | string | 否   |              | 米大师支付版本参数 默认为虚拟支付 1.0 midasPaymentVersion: 2 虚拟支付 2.0                                                                                                             |
| desc                | string | 是   |              | 客服推送给玩家消息上的描述 （字段解释见[图 1](#图1)） 示例 点击我充值 XX 元                                                                                                           |
| thumb_url           | string | 是   |              | 示例图片 http://cdn.11h5.com/share/icon/lianjie3.jpg （字段解释见[图 1](#图1)                                                                                                         |
| cardTitle           | string | 否   | 游戏名称     | 玩家推送给客服卡片上的图片 （字段解释见[图 1](#图1)）                                                                                                                                 |
| txid                | string | 否   |              | 商户订单号，最多 64 个字节。如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。                                            |
| userdata            | string | 否   |              | 商户自定义参数，最多 128 个字节。如果不为空，则会在通知发货接口中返回给商户，请对该数据进行 url 转义，否则可能会丢失数据，可以参考 js 的 encodeURI 或者 php 中的 urlencode 进行转义。 |

##### 示例代码

```js
sdk
  .pay({
    gname: "游戏名称",
    product_id: "商品ID",
    product_count: "1",
    serverid: "1",
    env: 0,
    cardImageUrl: "https://cdn.kxtoo.com/guessword/qst/unity/iospay.png",
    title: "点我进行充值",
    desc: "点击我充值6元",
    thumb_url: " http://cdn.11h5.com/share/icon/lianjie3.jpg",
  })
  .then(() => {
    // 成功打开客服会话
    // 或者成功显示了支付二维码
  })
  .catch(() => {
    // 失败
  });
```

##### 图1

<img :src="$withBase('/images/wxa/chongzhi.png')" alt="Image from dependency">

### 安卓支付

#### 安卓米大师支付准备

::: danger
此功能在游戏上线前必须接入
:::

- 申请小游戏虚拟支付（申请微信支付前需要绑定微信商户，绑定操作需要找对应运营进行绑定。）
- 向微信发送邮件申请虚拟支付--米大师。
- 支付环境说明（沙箱环境和正式环境的游戏币不互通）
  - 沙箱环境: 虚拟支付还未发布前使用的环境，也就是测试环境。
  - 正式环境: 线上环境。

#### 获取虚拟支付相关参数

虚拟支付申请成功后会获得以下参数，将申请到的虚拟支付相关参数发给运营进行配置。

| 参数名称    | 参数描述                                              | 举例                             |
| :---------- | :---------------------------------------------------- | :------------------------------- |
| appid       | 小游戏的 appid，wx 开头的一个字符串                   | wx123abc789                      |
| offer_id    | 支付应用 ID(米大师应用 id),在发起米大师支付时需要用到 | 12345678                         |
| sandbox_key | 沙箱 AppKey,支付测试环境使用的 key                    | zNLgAGgqsEWJOg1nFVaO5r7fAlIQxr1u |
| key         | 现网 AppKey,支付正式环境使用的 key                    | onLQZS9y9Rv7QqcBol4sMIo85dxuLIjE |

#### 配置人民币兑换游戏币比例

- 在商户平台游戏配置中将兑换比例配置成 1:100。
- 在[微信公众平台](https://mp.weixin.qq.com/)，修改虚拟支付游戏币兑换比例为 1:100，即 1 个游戏币=1 分钱人民币。

#### 配置米大师分区信息

`运营过程中如果商户后台添加了商品，这里也需要进行同步。`

将商户平台上配置的商品 id 作为货币分区 ID，商品名称作为分区名称，配置在微信商户平台中，配置完成后点击发布沙箱环境和发布正式环境。

<img :src="$withBase('/images/wxa/config_midas.png')" alt="Image from dependency">

#### 支付环境配置

在商户后台，在游戏的额外参数中配置沙箱环境（虚拟支付对接中，只能在沙箱环境中测试）。

虚拟支付对接完成后，开放正式环境后，删除这个配置

| 参数名称    | 参数描述                                                     |
| :---------- | :----------------------------------------------------------- |
| midasPayEnv | 米大师环境变量<br>0 或者不配置表示正式环境<br>1 表示沙箱环境 |

<img :src="$withBase('/images/wxa/midas_pay_env.png')" alt="Image from dependency">

#### 支付开关

在商户平台，在游戏的额外参数中配置开关，如果不配置默认是开启支付。

| 参数名称 | 参数描述                                               |
| :------- | :----------------------------------------------------- |
| closePay | 支付开关<br>0 或者不配置表示打开支付<br>1 表示关闭支付 |

##### 获取安卓虚拟支付环境

sdk.getAndroidPayEnv(string version).then(Object res)

##### 参数

string version
小游戏版本号，可以获取到对应版本号的支付配置

##### 返回参数 Object res

| 属性        | 类型    | 说明                                                         |
| ----------- | ------- | ------------------------------------------------------------ |
| midasPayEnv | number  | 米大师环境变量<br>0 或者不配置表示正式环境<br>1 表示沙箱环境 |
| closePay    | boolean | false: 不能支付 , true: 可以支付                             |

##### 示例代码

```js
const { midasPayEnv, closePay } = await sdk.getAndroidPayEnv("version0.0.1");
```

#### 支付

sdk.pay(Object object).then(Object res).catch(Object err)

##### 参数

[Object object](#参数payObject)

```js
sdk
  .pay({
    offerId: "米大师应用ID",
    product_id: "商品ID",
    product_count: 1,
    serverid: "1",
    env: 0,
  })
  .then((res) => {
    // 支付成功
  })
  .catch((res) => {
    // 支付失败
  });

// 支付错误码，将会通过throw 抛出
{
  error: 0; //操作成功
  1; //缺少参数
  2; //系统错误
  3; //token失效,请尝试重新登录
  4; //该appid未配置支付参数
  5; //商品不存在
  6; //游戏不存在
  7; //微信系统繁忙，请稍后再尝试购买
  8; //游戏币余额不足
  9; //缺少参数token
  10; //缺少参数appid
  11; //缺少参数product_id
  12; //购买数量错误，必须在1-1000之间
  13; //虚拟支付还在接入中，请使用沙箱环境测试
  14; //缺少参数订单号
  15; //订单不存在
  16; //订单已处理
  17; //支付用户和下单用户不一致
  18; //订单正在处理中，将在稍后到账
  19; //缺少参数gameid
  20; //游戏币兑换比例未配置，请在商品平台上进行配置
}
```

- 1001 requestMidasPayment:fail 请求参数错误(pf or zoneid not valid)
  - 未配置分区信息，请查阅[配置米大师分区信息](#配置米大师分区信息)
- 1016 requestMidasPayment:fail 系统繁忙，请稍后再试
  - 未发布沙箱环境或者生产环境

### 专属客服

::: danger
此功能在游戏上线前必须接入 向运营提供 svip 图标(图 1，2)(124\*\124)和客服会话卡片图标(240\*200)（图 4）素材
切记：SVIP 不能出现福利/奖励等字眼
:::

<!-- ## 1.Svip 面板展示

<strong class="red">重要：(需要接入方可以分别控制 体验版和线上版 svip 入口的显示和隐藏，)</strong>

<strong class="red">①SVIP 需要 CP 做如下图一到图三，三张图的美术素材，与我方运营沟通，确认素材尺寸大小。（美术素材，切记不能有一点暴露）</strong>

<strong class="red">② 准备一份豪华大礼包（非通码），礼包内容需要有吸引力。</strong>

Svip 图标需要在主城和战斗两个界面都正常显示<strong class="red">用户是 svip 显示图标，不是 svip 不显示图标，游戏内充值达到 svip 条件返回游戏立刻显示 svip 图标</strong>。

注：

① 如果没有主城之类的游戏需跟运营沟通显示的地方

② 通过互通链接进入游戏，也要显示 SVIP 按钮。

![Image](/images/wxa/svipbg.png) 图 1

1）用户成为 svip，点击 svip 图标，可以打开 svip 面板（同图 1）：

需要动态绘制 用户 UID，客服电话号码（），复制按钮 <strong class="red">【用户充值达标后，游戏中要及时更新，不需要重新登录游戏才更新】</strong>

[代码示例 svip](./wxa_main.md#代码示例svip) -->

#### Svip 图标状态

1）用户未成为 Svip 之前，无法看到 svip 图标

<img :src="$withBase('/images/wxa/svip1.png')" alt="Image from dependency">图 2

2）用户成为 Svip，充值完成后，Svip 图标状态立即更新图标状态（图标上显示可领取字样）：

<img :src="$withBase('/images/wxa/svip2.png')" alt="Image from dependency">图 3

3）用户成功添加客服，并被我方客服在后台标记为已添加后，用户重新刷新游戏或者点击 SVIP 图标，图标状态变成图 3（无可领取字样）状态。

4）客服会话卡片图标

<img :src="$withBase('/images/wxa/kefu.png')" alt="Image from dependency">图 4

#### 代码示例 svip

```js

// 该接口目前仅支持 Egret & Laya 引擎 （方法一）
// 玩家游戏内充值达到条件返回游戏状态需要更新，在每次游戏热启动需要手动调用一次sdk.createSVIP
    const [res, err] = await sdk.createSVIP(opt: {
        container: any // '当前上下文',
        x: number // 'svip icon放置的横坐标',
        y: number //'svip icon放置的纵坐标',
        width: number // 'svip icon的宽度',
        height: number // 'svip icon的高度',
        drag?: boolean // 'true可以拖拽 false不可以拖拽（可选参数：默认为false）'
        engine?: number // 1：Laya 0:egret(可选参数：当前游戏引擎默认为0 )',
    });
    // 返回成功 res: bitmap对象  默认为隐藏状态（res.visible = false） 需要在挂机和主城手动设置为true
    // if (res) res.visible = true
    // 返回失败 res：null

// 自己画icon （方法二）
// 点击svipIcon后的数据统计 （必须接入 -- 点击后调用）
sdk.svipClickStat();
const [ res, err ] = await sdk.getSvipInfo();
/**
    * @param {boolean} isSvip // 用户是否是svip  false：否  true：是
    * @param {boolean} isOpen // 游戏是否打开svip功能  false：否  true：是
    * @param {boolean} isAdd // 用户是否已经添加了svip false：否  true：是
    * @param {{ svipNewIcon: string, svipActiveIcon:string, customerServiceConversationUrl: string }} ext
*/
const { isSvip, isOpen, ext, phone } = res
const { svipNewIcon, svipActiveIcon, customerServiceConversationUrl} = ext
// svipNewIcon  svip不可领取状态iconUrl -- 图 3
// svipActiveIcon   svip可领取状态iconUrl -- 图 4

// 点击svipIcon后调用
sdk.openCustomerServiceConversation({
  action: 'workWeixinKf', // 固定值
  imgUrl: customerServiceConversationUrl
}).then().catch();

```

## 游戏内有分享功能必接入

#### 分享上报

::: danger
游戏内有分享功能必接入,玩家分享完成，前端调用此方法。
:::

```js
sdk.mpShareStat();
```

#### 获取分享参数

获取一个参数对象，该对象里面的参数均需要追加到分享的 path 路径上

```js
const query = sdk.getShareParams();
/*
    返回:
    { 
      chid:number
      fuid:number
      subchid:string
      suid:number
      ...一些游戏自定义的参数 
    }
    如果小游戏测分享需要带自己游戏自定义的参数 上述参数只需拼接chid fuid subchid suid 
    需要按wx官方分享接口拼接相关参数
*/
```

## 选接

### 关注

::: danger
【根据游戏情况，自行判断是否接入，目前接入此功能需慎重，关注页面切记不能出现福利和奖励物品】
（可用公众号：增加玩家福利/游戏入口/礼包获取/uid 查询等需运营提前申请同名公众号/或使用现有公众号；根据 sdk.displayFocus 判断是不是要显示公众号
:::

```javascript
const ret = await sdk.displayFocus();
// 显示关注二维码 ret = true
// 不显示关注二维码 ret = false
// 接口返回错误  { error: xxx }
```

- 游戏内需要增加一个领取关注礼包的 icon，icon 要醒目，开放等级尽量在玩家刚进区就开放，玩家没有关注，点击 icon 引导玩家关注，如果已经关注，则直接领取奖励；
- 关注礼包界面：显示礼包奖励内容；玩家点击复制公众号，玩家搜索公众号名称关注；或者放公众号二维码，玩家扫码关注；
- 玩家关注公众号后，从公众号底部菜单进入游戏（会带一个参数 attention=1，可在 onShow 的回调 options 的 query 对象下获取，也可以直接通过 sdk.isFocus()判断），判断有此参数即可激活礼包，直接发放到玩家邮箱，玩家滚服可以直接激活关注礼包
- 此页面不能出现福利/物品奖励等字眼/道具展示；

界面示例：

<img :src="$withBase('/images/wxa/guanzhu.png')" alt="Image from dependency">图 4

### 是否公众号打开

判断是否是通过公众号菜单入口打开的游戏
注意：热启动应该判断

```js
const res = sdk.isFocus(); // 返回一个布尔值
if (sdk.isFocus()) {
  // 说明用户是通过公众号菜单进入
  // 可以发送关注奖励
}
```

### 获取启动参数

获取解析后的启动参数

```js
const query = sdk.getQuery();
```

### 绑定手机

::: danger
重要：(根据游戏情况选择接入)
① 需要提供一张完善资料的页面，如下图五，与我方运营沟通尺寸情况，切记不可有暴露的）
② 需准备完善资料的奖励，价值 100-300 元。
用户成为 Svip 后，成功添加客服，客服给予绑定手机链接；
（可直接微信中打开，也可以浏览器中打开，打开后如下图）
:::

<img :src="$withBase('/images/wxa/bindphoneh5.png')" alt="Image from dependency">

::: danger
PS：注意绑定手机功能，所有界面文字不能出现绑定手机字样，统一叫：完善资料
绑定成功，弹出绑定成功的提示框，提示用户去游戏邮箱中领取奖励：
:::

<img :src="$withBase('/images/wxa/jl.png')" alt="Image from dependency">

::: danger
绑定手机奖励在游戏多个服的情况下，各个服都可以领取奖励；绑定成功后，回到游戏，可以及时刷新或者切换到邮箱界面更新奖励领取状态：
:::

<img :src="$withBase('/images/wxa/gamejl.png')" alt="Image from dependency">
<img :src="$withBase('/images/wxa/gamejl2.png')" alt="Image from dependency">

::: danger
PS：奖励邮件的标题和邮件内容，都要叫 '完善资料奖励'

邮件标题：完善信息奖励
邮件内容：亲爱的玩家，以下是您本次完善信息的奖励，请您查收。
:::

### 检查绑定手机状态（用于发送绑定手机奖励）

```js
/*
1、小游戏侧，绑定完成需要2次进入游戏才能发奖励（大部分玩家应该都是这个流程）
2、H5侧，如果当前玩家的绑定奖励没有发放，直接发放（适合绑定手机后再没进过小游戏的用户）
备注：H5入口，不需要提供检测玩家是否绑定的接口，因为这个入口没有绑定手机是进入不了游戏的。
注意：游戏内不能出现 ‘手机绑定’敏感词
*/
//进入游戏时再合适的地方调用
const [res, err] = await sdk.checkBind();
/*
    // 绑定成功 
    res:{ error:0 }  进入游戏弹出相关提示 直接发奖励到游戏邮箱
    提示格式 
        标题：完善信息奖励
        内容：亲爱的玩家，以下是您本次完善信息的奖励，请您查收
    // 绑定失败 
    err:{
            error:10004  token失效
            error:10010  参数错误
            error:700002 手机号未绑定
        }  
*/
```

### 判断前置游戏是否开启

```js
const isOpen = await sdk.isOpenPreGame();
// isOpen: true 开启 ，false 关闭
```

### 用户完成前置小游戏

::: danger
仅在前置游戏开启情况下调用
:::

```js
sdk.achievePreGame();
```

## 休闲游戏接入

#### 获取浮动广告

::: warning
根据游戏情况接入，休闲小游戏必接
:::

浮动广告每隔 30 秒就会自动更新一个新的广告，点击广告之后则会立刻更新一次

```js
const floatAd = await sdk.getRandAd({}, (res) => {
  // 获取广告数据
  const [{ icon, type, intervals, appid }] = res;

  // icon 是一个数组，可能存在多张图片
  // type 当前的广告类型，用于广告跳转
  // intervals icon轮换间隔，存在多张icon的情况下，icon需要按照此值进行轮换
  // appid 当前广告的appid ，用于广告跳转
});

// 调用下方方法，将停止该浮动广告的自动更新，比如面板销毁，减少内存占用。
floatAd.stop();
```

#### 获取精品推荐广告

::: danger
此功能仅休闲小游戏接入
:::

::: warning
由于统计需要，玩家每点击一次打开精品游戏面板的 icon，都需要重新获取广告数据
:::

```js
const data = await sdk.getRandAd({ type: 1 });

// data为一个广告数组
data.forEach((item) => {
  // 获取广告数据
  const [{ icon, type, intervals, appid, title }] = item;

  // icon 是一个数组，可能存在多张图片
  // type 当前的广告类型，用于广告跳转
  // intervals icon轮换间隔，存在多张icon的情况下，icon需要按照此值进行轮换
  // appid 当前广告的appid，用于广告跳转
  // title 当前广告的名称，主要用于显示在图标下方
});
```

#### 获取猜你喜欢广告

由于统计需要，玩家每点击一次打开猜你喜欢面板，都需要重新获取广告数据

```js
const data = await sdk.getRandAd({ type: 2 });

// data为一个广告数组
data.forEach((item) => {
  // 获取广告数据
  const [{ icon, type, intervals, appid, title }] = item;

  // icon 是一个数组，可能存在多张图片
  // type 当前的广告类型，用于广告跳转
  // intervals icon轮换间隔，存在多张icon的情况下，icon需要按照此值进行轮换
  // appid 当前广告的appid，用于广告跳转
  // title 当前广告的名称，主要用于显示在图标下方
});
```

#### 获取积分墙广告

由于统计需要，玩家每点击一次打开积分墙游戏推荐面板，都需要重新获取广告数据

```js
const data = await sdk.getRandAd({ type: 3 });

// data为一个广告数组
data.forEach((item) => {
  // 获取广告数据
  const [
    {
      icon,
      type,
      intervals,
      appid,
      title,
      slogan,
      quantity,
      hasgot,
      cate,
      isReady,
      gameid,
    },
  ] = item;

  // icon 是一个数组，可能存在多张图片
  // type 当前的广告类型，用于广告跳转
  // intervals icon轮换间隔，存在多张icon的情况下，icon需要按照此值进行轮换
  // appid 当前广告的appid，用于广告跳转
  // title 当前广告的名称，主要用于显示在图标下方
  // slogan 标语
  // quantity 奖励数量
  // cate 奖励类型
  // hasgot 奖励是否已经领取
  // isReady 奖励是否可以领取
  // gameid 游戏ID，领取奖励的时候需要此值
});
```

#### 领取积分墙奖励

```js
// 领取奖励需要传入积分墙对应广告的游戏ID
const { error } = await sdk.getAdAward(gameid);

// error 为0 即为领取成功
```

#### 广告跳转

点击广告后，调用该方法，进行广告跳转
此方法兼容小游戏 SDK2.2.0 及以上版本的 navigateToMiniProgram 的直接跳转和低版本自动调用 wx.previewImage 展示小程序码的方式

```js
sdk.navigateToMiniProgram({
  appid, //必填字段，代表目标小程序的appid，此appid从广告数据里面获取
  type, //必填字段，代表广告类型， 此type从广告数据里面获取
});
```

#### 广告代码特效示例

此代码示例，用来表现浮动广告，精品推荐，猜你喜欢，积分墙广告的 icon 动态效果，仅供参考

- 通过浮动广告获取到的素材，默认宽高为 158\*198，项目依实际进行缩放
- 通过精品推荐，猜你喜欢，积分墙获取到的广告素材，默认宽高均为 136\*136，项目依实际进行缩放

```js

/**
 *
 * @param {egret.DisplayObject} dis 缓动显示对象
 * @param {number} rotation 缓动角度
 * @param {number} duration 缓动时间
 * @param {number} waitTime 等待时间
 */

export function playAdEffect(dis: egret.DisplayObject, rotation: number = 25, duration: number = 150, waitTime: number = 4000): void {
    egret.Tween.get(dis, {loop: true})
        .wait(waitTime)
        .to({rotation: -1 * rotation}, duration / 2)
        .to({rotation: rotation}, duration)
        .to({rotation: -1 * (rotation * 0.8)}, duration * 0.8)
        .to({rotation: (rotation * 0.8)}, duration * 0.8)
        .to({rotation: 0}, (duration * 0.8) / 2);
}

private setImg(){
  this._adIdx++;
  if(this._adIdx >= this._adList.length){
    this._adIdx = 0;
  }
  this.advBtn.source = this._adList[this._adIdx];
}

private removeAdvEffect() {
  this.advBtn.visible = false;
  this.advBtn.rotation = 0;
  this.advBtn.anchorOffsetX = this.advBtn.width / 2;
  this.advBtn.anchorOffsetY = this.advBtn.height / 2;
  this.advBtn.x = this.advBtn.x + this.advBtn.width / 2;
  this.advBtn.y = this.advBtn.y + this.advBtn.height / 2;
  egret.Tween.removeTweens(this.advBtn);
}

private _adIdx:number;
private _adList:string[];

private showPlayAd(res){

    //res---平台回调的res
    this.removeAdvEffect();

    if(res.icon.length == 1){ //只有一张图片

        this.advBtn.source = res;

        //使用抖动效果
        this.playAdEffect(this.advBtn);

    }else if(res.icon.length == 2){ //多张图片采用帧动画

        let interval = res.intervals; //每帧间隔 (毫秒)
        this.advBtn.visible = true;
        this._adList = res.icon; //图片列表
        this.advBtn.source = this._adList[0];
        this._adIdx = 0;
        egret.Tween.get(this.advBtn,{loop:true}).call(this.setImg, this).wait(interval);
    }
}

```

> 抖动特效实际效果参考

<img :src="$withBase('/images/wxa/doudong.gif')" alt="Image from dependency">

<br/>

> 帧动画实际效果参考

<img :src="$withBase('/images/wxa/zhendonghua.gif')" alt="Image from dependency">

### 激励广告

初始化激励广告 预加载激励广告

##### 方法

```ts
// 初始化激励广告
sdk.initRewardedVideoAd(AdId:string):undefined
// 展示激励广告
sdk.showRewardedVideoAd(): Promise<string>
```

> 以 Promise 风格 调用：支持

##### 参数

LoginResp

| 字段名称 | 字段类型 | 是否必填 | 字段说明              |
| :------- | :------- | :------- | :-------------------- |
| AdId     | string   | 是       | 微信后台流量主广告 id |

##### 示例代码

```js
// 初始化
const adId = "adunit-xxxxxxxxxx";
sdk.initRewardedVideoAd(adId);

// 展示广告
sdk
  .showRewardedVideoAd()
  .then((e) => {
    console.log(e, "====>可以领取奖励");
  })
  .catch((e) => {
    console.log(e, "====>无法领取奖励");
  });
```

### 关键行为上报

sdk.pivotalBehaviorsStat()

> 以 Promise 风格 调用：支持

##### 功能说明

上报用户行为到 mp 统计后台

##### 示例代码

```js
sdk.pivotalBehaviorsStat().then(([res, err]) => console.log(res, err));
```

##### sdk.pivotalBehaviorsStat 返回参数

Array Object

```js
返回成功[(res, null)];
返回失败[(null, res)];
```

[[toc]]
