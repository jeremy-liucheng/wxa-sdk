# 前端接入准备

## 登陆

### 登录

:::danger
需要在途悦 sdk 登陆后调用，需要用到途悦的 userId 和 openId
:::

- 地址：`https://wxlogin.${项目域名}/polymerize`

请求方式：POST

| 字段名称  | 字段类型    | 是否必填 | 字段说明                                                    |
| --------- | ----------- | -------- | ----------------------------------------------------------- |
| cmd       | String      | 是       | 固定填写 miniproLogin                                       |
| appid     | String      | 是       | 小游戏应用 id                                               |
| code      | String      | 是       | 抖音登录 cdoe                                               |
| scene     | Number      | 是       | 场景值                                                      |
| device    | Number      | 是       | 安卓=1，ios=2                                               |
| gameid    | Number      | 是       | 小游戏 gameid                                               |
| chid      | Number      | 是       | 广告启动参数                                                |
| subchid   | Number      | 是       | 广告参数 如果启动参数没有 chid 或者 chid=0，subchid = scene |
| v         | Number      | 是       | 当前时间戳                                                  |
| ...params | [key:value] | 是       | 其余广告启动参数                                            |

返回值

```js
{
  uid: 30000000;
  token: "xxxxxxxx";
}
```

### 用户归属

::: danger
只在启动参数有 suid 或者 cp_adStat 调用
:::

- 地址：`https://adstat.${项目域名}/stat`

请求方式：POST

| 字段名称  | 字段类型    | 是否必填 | 字段说明     |
| --------- | ----------- | -------- | ------------ |
| cmd       | String      | 是       | updateBelong |
| token     | String      | 是       | 用户 token   |
| uid       | number      | 是       | 平台 uid     |
| ...params | [key:value] | 是       | 启动参数     |

返回值

```js
{
  error: 4;
  errmsg: "操作成功";
}
```

### 游戏在线统计

- 地址：`https://adstat.${项目域名}/stat`

请求方式：POST

| 字段名称 | 字段类型 | 是否必填 | 字段说明       |
| -------- | -------- | -------- | -------------- |
| cmd      | string   | 是       | 固定填写 alive |
| token    | string   | 是       | 用户 token     |
| gameid   | number   | 是       | 小游戏 id      |
| uid      | number   | 是       | 用户 平台 id   |
| v        | Number   | 是       | 当前时间戳     |

五秒调用一次

## 支付

### 支付准备

:::danger
ios 需要投放之后 才能申请开白，目前没有 ios 支付；如果是 ios 端 要屏蔽充值的

部分渠道会有支付金额档位限制，请注意适配，如 B 站、微信小游戏订单金额，仅支持传入[ 1 , 3 , 6 , 8 , 12 , 18 , 25 ,30 , 40 , 45 , 50 , 60 , 68 , 73 ,78 , 88 , 98 , 108 , 118 , 128 ,148 , 168 , 188 , 198 , 328 , 648 ,998 , 1498 , 1998 , 2498 , 2998 ]
:::

商户后台配置商品，如下：

<img :src="$withBase('/images/wxa/shop.png')" alt="Image from dependency">

### 获取订单 id

:::danger
获取订单 id 之后调用
TYSDK.Pay(new TYPayParam()
{
orderId = "19988811",
}
)
:::

- 地址：https://login.${项目域名}/pay/bytedance/order.php`

请求方式：GET

请求参数

| 字段名称      | 字段类型 | 是否必填 | 字段说明                                                                                                                                                                                                    |
| :------------ | :------- | :------- | :---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| token         | string   | 是       | 用户 token                                                                                                                                                                                                  |
| uid           | number   | 是       | 用户平台 id                                                                                                                                                                                                 |
| gameid        | number   | 是       | 平台游戏 id                                                                                                                                                                                                 |
| device        | number   | 是       | 安卓=1，ios=2                                                                                                                                                                                               |
| product_id    | number   | 是       | 商品 id                                                                                                                                                                                                     |
| product_count | number   | 是       | 商商品数量                                                                                                                                                                                                  |
| gameid        | number   | 是       | 游戏 id                                                                                                                                                                                                     |
| serverid      | number   | 是       | 区服 id                                                                                                                                                                                                     |
| v             | number   | 是       | 当前时间戳                                                                                                                                                                                                  |
| txid          | string   | 否       | 游戏侧传入 sdk 做透穿 商户订单号，最多 64 个字节。如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。                                            |
| userdata      | string   | 否       | 游戏侧传入 sdk 做透穿 商户自定义参数，最多 128 个字节。如果不为空，则会在通知发货接口中返回给商户，请对该数据进行 url 转义，否则可能会丢失数据，可以参考 js 的 encodeURI 或者 php 中的 urlencode 进行转义。 |

返回值

```js
{
  buy_quantity: 600; // 金币购买数量，金币数量必须满足：金币数量*金币单价 = 限定价格等级（详见下方 buyQuantity 限制说明。开发者可以在字节小游戏平台的“支付”tab 设置游戏币单价）
  trans_id: 222222222; // 支付平台订单号
}
```

## MP 统计

### 分享上报

- 地址：`https://adstat.${项目域名}/stat`

请求方式：POST

| 字段名称 | 字段类型 | 是否必填 | 字段说明       |
| -------- | -------- | -------- | -------------- |
| cmd      | string   | 是       | 固定填写 share |
| token    | string   | 是       | 用户 token     |
| gameid   | number   | 是       | 小游戏 id      |
| uid      | number   | 是       | 用户 平台 id   |
| v        | Number   | 是       | 当前时间戳     |

### 有效创角上报

- 地址：`https://adstat.${项目域名}/stat`

请求方式：POST

| 字段名称 | 字段类型 | 是否必填 | 字段说明               |
| -------- | -------- | -------- | ---------------------- |
| cmd      | string   | 是       | 固定填写 effectiveRole |
| token    | string   | 是       | 用户 token             |
| gameid   | number   | 是       | 小游戏 id              |
| uid      | number   | 是       | 用户 平台 id           |
| v        | Number   | 是       | 当前时间戳             |

## 激励广告

### 激励广告统计

- 地址：`https://platform.${newDomain}/stat/stat`

请求方式：POST

| 字段名称 | 字段类型 | 是否必填 | 字段说明                                                                                                       |
| -------- | -------- | -------- | -------------------------------------------------------------------------------------------------------------- |
| cmd      | string   | 是       | 固定填写 adStat                                                                                                |
| act      | string   | 是       | 正常播放结束，可以下发游戏奖励= wxAdOnCloseIsEndedTrue ,播放中途退出，不下发游戏奖励 = wxAdOnCloseIsEndedFalse |
| token    | string   | 是       | 用户 token                                                                                                     |
| gameid   | number   | 是       | 小游戏 id                                                                                                      |
| uid      | number   | 是       | 用户 平台 id                                                                                                   |
| v        | Number   | 是       | 当前时间戳                                                                                                     |

## 获取分享参数

```ts
declare namespace GC {
  type Params = { [key: string]: any };
}

export function GetShareParams(): GC.Params {
  const { uid } = GetData();
  const { params } = ParseOptions();

  // 替换成自己的uid
  params.suid = uid;
  params.fuid = uid;

  // 移除关注的参数
  delete params.attention;
  delete params.from;

  return params;
}
```

## 获取启动参数

```ts
declare namespace GC {
  interface OnshowOptions {
    /** 场景值 */
    scene: number;
    /** 查询参数 */
    query: Params;
    /** shareTicket */
    shareTicket: string;
    /** 当场景为由从另一个小程序或公众号或App打开时，返回此字段 */
    referrerInfo: ReferrerInfo;
  }
  interface OptionsParseResult {
    scene: number;
    params: Params;
  }
}

/** 解析启动参数 */
export function ParseOptions(option?: GC.OnshowOptions): GC.OptionsParseResult {
  const ops = option || tt.getLaunchOptionsSync();
  // 获取场景值 获取参数
  const { query, scene } = ops;
  const params: GC.Params = {};
  // 解析参数
  Object.entries(query).forEach((v) => {
    const [o, value] = v;
    // 统一去掉key值里面包含的?
    const key = o.replace("?", "");

    switch (key) {
      // 小程序码生成的自定义参数，转义并进行参数分解
      case "scene":
      case "adparam": // QQ小程序携带的参数
        decodeURIComponent(value)
          .split(",")
          .forEach((kv) => {
            const [k, val] = kv.split(":");
            params[k] = val;
          });
        break;
      default:
        // 排除appid, gameid
        if (key !== "appId" && key !== "gameid") {
          params[key] = value;
        }
        break;
    }
  });

  // 尝试去获取缓存里面是否有chid
  // 主要是为了避免第一次没有成功进入游戏，二次进入的时候因为有可能是从别的入口进参数会丢失的情况
  // 参数里面没有chid，
  if (!+params.chid) {
    params.chid = GetCookie("mp_chid") || 0;

    const subchid = GetCookie("mp_subchid");

    // eslint-disable-next-line eqeqeq
    if (params.chid == 0) {
      params.subchid = String(scene);
    }
    if (subchid) {
      params.subchid = subchid;
    }
  }

  if (!params.fuid) {
    const fuid = GetCookie("mp_fuid");
    if (fuid) {
      params.fuid = fuid;
    }
  }
  if (!params.suid) {
    const suid = GetCookie("mp_suid");
    if (suid) {
      params.suid = suid;
    }
  }
  return {
    scene: scene as any,
    params,
  };
}
```
