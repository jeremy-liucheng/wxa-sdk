<style>
.red{color:red;}
</style>

# 前端接入准备

<strong class="red">sdk 调用时机（请严格遵守）：请在小游戏头包解压完，小游戏侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.</strong>

联系运营确定使用的域名，如运营提供的为`xxxx.com`，那么此项目 sdk 内用到的所有协议域名都是`xxxx.com`（下文中所有`xxxx.com`都是这个域名），下面将正式开始~

请将以下域名，加入到小游戏 **request** 白名单

> `https://adapi.xxxx.com,https://act.xxxx.com,https://newintegral-wall.xxxx.com,https://adstat.xxxx.com,https://wxlogin.xxxx.com,https://platform.xxxx.com,https://api.xxxx.com,https://login.xxxx.com`

请将以下域名，加入到小游戏 **downloadFile** 白名单

> `https://api.xxxx.com,https://act.xxxx.com`

### [下载 sdk](/wxa-sdk/sdk/byte/byte_wxa.0.0.1.zip)

## 必接

### 初始化 sdk

```js

import Sdk from '本地sdk路径';

const sdk = new Sdk({
    // 必填字段，传入当前游戏ID
    gameid: gameid,
    // 必填字段，当前小游戏的appid
    appid: appid,
    // 选填字段，联系运营提供
    domain?: 'xxx.com',
})

```

### 登录

```js
// uid默认为0，token为空 （token有效期为12个小时）
// 如果登录成功，将可获得用户的uid，token信息
// token会缓存在storge里面 下次登陆验证token有没有过期 （注意：删除uid token如果还在有效期会返回老的uid）
// 默认为h5互通版本
sdk
  .login()
  .then(({ uid, token }) => {
    // 登录成功
  })
  .catch((res) => {
    // 登录失败
  });
```

### 更新用户信息

静默授权，无法获取用户的头像、昵称等敏感信息

如果项目需要获取用户的个人信息，需要调用此方法，进行信息同步

```js

const userInfo = await sdk.updateUserInfo()

UserInfo {
    /** 用户昵称 */
    nickName: string;
    /** 用户头像图片的 URL。
     * URL 最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，
     * 0 代表 640x640 的正方形头像，46 表示 46x46 的正方形头像，剩余数值以此类推。默认132），
     * 用户没有头像时该项为空。若用户更换头像，原有头像 URL 将失效。 */
    avatarUrl: string;
    /** 用户性别 */
    gender: 0 | 1 | 2;
    /** 用户所在国家 */
    country: string;
    /** 用户所在省份 */
    province: string;
    /** 用户所在城市 */
    city: string;
    /** 用户语言，目前为空 */
    language: ''
}

```

### 联系客服

<strong class="red">此功能在游戏上线前必须接入，客服入口 icon 要放在显眼位置！！</strong>

```js
sdk.openCustomerServiceConversation({
  image: string, // 图标url
  style: {
    left: number, // 横坐标
    top: number, // 纵坐标
    width: number, // 图标宽度
    height: number, // 图标高度
    borderRadius?: number, // 边框圆角 （可选 默认 4）
    borderColor?: '#ffffff', // 边框颜色 （可选: 默认 '#ffffff'）
    backgroundColor?: '#ffffff', // 边框颜色 （可选 默认 '#ffffff'）
    borderWidth?: number, // 边框宽度 （可选 默认 1）
  },
})
  .then(res => {
    console.log(res, '====>创建成功');
  })
  .catch(err => {
    console.log(err, '====>创建失败');
  });
```

## 有内购功能游戏必接入

### 苹果支付

#### 支付准备

<strong class="red">此功能在游戏上线前必须接入</strong>

商户后台配置商品，如下：

<img :src="$withBase('/images/byte/shop.png')" alt="Image from dependency">

#### 支付

使用前请前往
https://microapp.bytedance.com/game/tt05f973449ac0036302/traffic-other/customer
绑定抖音 IM 客服后续充值相关问题用户都会通过该客服进行咨询（请勿用绑定的抖音号进行支付测试，会导致无法拉起客服界面）（本步骤未完成报错：fail im disable ）

```js
// 第一步：点击充值按钮获取订单信息
  const [ret, err] = await sdk.getOrderInfo({
      product_id: number,
      product_count: number,
      /** 当前的区服  */
      serverid: '1',
      /**
       * 商户订单号，最多64个字节。
       * 如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。
       */
      txid?: string,
      /** 商户自定义参数，最多128个字节。
       * 如果不为空，则会在通知发货接口中返回给商户，请对该数据进行url转义，否则可能会丢失数据，
       * 可以参考js的encodeURI或者php中的urlencode进行转义。 */
      userdata?: string,
  })
  /**
   * ret成功返回 {"error":0,"errmsg":"操作成功","trans_id":number,"buy_quantity":100}
   * 失败返回 null
   */

  /**
   * 第二步：由于抖音ios支付需要原生点击事件 订单信息获取成功后需要cp自行绘制二次确认按钮 在二次确认按钮的原生点击事件回调中调用 (抖音ios支付限制必须是点击回调，所以不是promise风格 不支持.then调用)
   */
  sdk.iosPay({
      buy_quantity: ret.buy_quantity, // 上一步订单信息中的buy_quantity
      trans_id: String(ret.trans_id), // 上一步订单信息中的trans_id
      callBack: (res => { // 回调函数
        // res成功回调 { error: 0, errmsg: '操作成功', trans_id }
        // res失败回调 {errmsg:string, error:number}
      })
  })
// 第三步: 抖音弹出进入客服提示 用户点击进入客服支付
```

- Tip: 用户成功拉起客服界面后即回调成功，未成功拉起客服界面即回调失败，与实际支付结果无关，是否支付成功请通过服务端接口查询余额自行保证。

- Tip：如果遇到 fail must invoke by user gesture 的报错信息，是因为该接口需要由点击行为触发，请确保在按钮的点击 回调中 使用本接口。
  - tt.createInteractiveButton 接口创建的 native 按钮，无法通过本限制。
  - 该限制预期在抖音 23.0.0 版本去除，这期间如果需要通过网络请求获取订单号无法避开这个限制的话，可以做成二次确认的形式，第一次点击通过请求获取订单号，第二次点击调用本接口。

### 安卓支付

#### 安卓支付准备

<strong class="red">此功能在游戏上线前必须接入</strong>

- 申请小游戏虚拟支付（申请抖音支付前需要绑定商户，绑定操作需要找对应运营进行绑定。）

#### 配置人民币兑换游戏币比例

- 在商户平台游戏配置中将兑换比例配置成 1:100。
- 在[抖音开发者后台](https://microapp.bytedance.com/)，修改虚拟支付游戏币兑换比例为 1:100，即 1 个游戏币=1 分钱人民币。

#### 支付配置

字节后台配置，如下：
<img :src="$withBase('/images/byte/byte_backend.png')" alt="Image from dependency">

小游戏后台填写支付回调地址：'https://login.{业务域名}.com/pay/bytedance/notify.php'

商户后台配置，如下：

<img :src="$withBase('/images/byte/commercial_backend_pay.png')" alt="Image from dependency">

appid、签名秘钥、服务器回调 Token
这三个参数需要平台这边管理员添加到商户后台游戏私有参数

#### 支付开关

在商户平台，在游戏的额外参数中配置开关，如果不配置默认是开启支付。

| 参数名称 | 参数描述                                               |
| :------- | :----------------------------------------------------- |
| closePay | 支付开关<br>0 或者不配置表示打开支付<br>1 表示关闭支付 |

#### 支付

```js

sdk.pay({
    /** 商户后台配置商品后，生成的商品ID */
    product_id: '商品ID',
    /**
     * 代表购买商品的数量，默认值为1。
     * 批量购买商品时使用，最大不要超过1000。
     * 如果不为空，则会在通知发货接口中返回此参数。
     */
    product_count: number,
    /** 当前的区服  */
    serverid: '1',
    /**
     * 商户订单号，最多64个字节。
     * 如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。
     */
    txid?: string,
    /** 商户自定义参数，最多128个字节。
     * 如果不为空，则会在通知发货接口中返回给商户，请对该数据进行url转义，否则可能会丢失数据，
     * 可以参考js的encodeURI或者php中的urlencode进行转义。 */
    userdata?: string,
}).then((res) => {
    // 支付成功
}).catch((res) => {
    // 支付失败
})

// 支付错误码，将会通过throw 抛出
{
    error:  0 //操作成功
            1 //缺少参数
            2 //系统错误
            3 //token失效,请尝试重新登录
            4 //该appid未配置支付参数
            5 //商品不存在
            6 //游戏不存在
            7 //系统繁忙，请稍后再尝试购买
            8 //游戏币余额不足
            9 //缺少参数token
            10 //缺少参数appid
            11 //缺少参数product_id
            12 //购买数量错误，必须在1-1000之间
            13 //虚拟支付还在接入中，请使用沙箱环境测试
            14 //缺少参数订单号
            15 //订单不存在
            16 //订单已处理
            17 //支付用户和下单用户不一致
            18 //订单正在处理中，将在稍后到账
            19 //缺少参数gameid
            20 //游戏币兑换比例未配置，请在商品平台上进行配置
}

```

## 游戏内有分享功能必接入

### 有效创角上报

双方约定当用户达到有效创角的条件，调用下面的方法进行上报

```js
sdk.effectiveStat();
```

### 获取分享参数

获取一个参数对象，该对象里面的参数均需要追加到分享的 path 路径上

```js
const query = sdk.getShareParams();
/*
    返回: 
    { 
      chid:number
      fuid:number
      subchid:string
      suid:number
      ...一些游戏自定义的参数 
    }
    如果小游戏测分享需要带自己游戏自定义的参数 上述参数只需拼接chid fuid subchid suid 
    需要按抖音官方分享接口拼接相关参数
*/
```

### 分享上报

玩家分享完成，调用此方法 （按照运营规定是否接入）

```js
sdk.mpShareStat();
```

### 获取启动参数

获取解析后的启动参数

```js
const query = sdk.getQuery();
```

## 选接

### 判断前置游戏是否开启

```js
const isOpen = await sdk.isOpenPreGame();
// isOpen: true 开启 ，false 关闭
```

### 用户完成前置小游戏

<strong class="red">仅在前置游戏开启情况下调用</strong>

```js
sdk.achievePreGame();
```
