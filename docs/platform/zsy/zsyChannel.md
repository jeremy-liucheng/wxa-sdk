# 接入准备

::: danger
sdk 调用时机（请严格遵守）：请在小游戏头包解压完，小游戏侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.
:::

联系运营确定使用的域名，如运营提供的为`xxxx.com`，那么此项目 sdk 内用到的所有协议域名都是`xxxx.com`（下文中所有`xxxx.com`都是这个域名），下面将正式开始~

请将以下域名，加入到小游戏 **request** 白名单

> `https://adapi.xxxx.com,https://act.xxxx.com,https://newintegral-wall.xxxx.com,https://adstat.xxxx.com,https://wxlogin.xxxx.com,https://platform.xxxx.com,https://api.xxxx.com,https://login.xxxx.com,https://zsy.coolthink.cn`

### [下载sdk](/wxa-sdk/sdk/zsy/sdk_wxa.2.5.0.zip?v=1)

### 始化sdk

```js
  import Sdk from '本地sdk路径';
  const sdk = new Sdk({
    appid:'xxxxxxx', // 微信后台appid
    gameid:'xxxxxxx', // 商户后台gameid
    zsyGame:'xxxxxxx', // 中手游游戏标识别 联系运营提供
    zsyChannel:'xxxxxxx', // 中手游渠道标识 联系运营提供
    zsySubChannel:'xxxxxxx', // 中手游子渠道标识 联系运营提供
  })
```

### 登陆

```js
  sdk.login().then(({uid,token,openid})=>{})
```

### 支付准备
<strong class="red">部分渠道会有支付金额档位限制，请注意适配，如B站、微信小游戏订单金额，仅支持传入[ 1 , 3 , 6 , 8 , 12 , 18 , 25 ,30 , 40 , 45 , 50 , 60 , 68 , 73 ,78 , 88 , 98 , 108 , 118 , 128 ,148 , 168 , 188 , 198 , 328 , 648 ,998 , 1498 , 1998 , 2498 , 2998 ]</strong>

### 支付

```typescript
  interface PayParams {
    /** 合服后的区服（玩家当前所在区服）ID */
    serverId: string;
    /** 合服后的区服（玩家当前所在区服）名称 */
    serverName: string;
    /** 合服前的区服 */
    oldServerId: string;
    /** 合服前的区服（旧区服）名称 */
    oldServerName: string;
    /** 角色id */
    roleId: string;
    /** 玩家名称 */
    roleName: string;
    /** 玩家当前游戏币（如金币或元宝或钻石等）数量 */
    coinNumber: number;
    /** 改名前角色名称，如果未改过名，则这里的值跟字段roleName的值一样，都为当前角色名称 */
    oldRoleName: string;
    /** 充值主题,字数不超过200个字符,例如:购买哪个等级或购买什么道具 */
    subject: string;
    /** 商品id */
    productId: number;
    /** 角色等级 */
    roleLevel: number;
    /** 角色vip等级 如果没有填0 */
    roleVipLevel: number;
    /** 游戏扩展字段,可为空值,长度不超过250个字符，该参数会在支付回调时原样返回 */
    gameExtend?: string;
  }
  sdk.pay(payParams:PayParams)
```

### 上报

```typescript
  interface ReportParams {
    /** 游戏类型（比如：武侠、休闲、竞技、枪战 等等)  */
    gameType: string;
    /** 合服后的区服（玩家当前所在区服）ID，没有就设为 '1'  */
    serverId: string;
    /** 合服前的区服（旧区服）ID，没有则默认为'1'，如果未合过服，则这里的值跟字段serverId的值一样，都为当前区服ID  */
    oldServerId: string;
    /** 合服后区服（玩家当前所在区服）名称，没有就设为 '1'  */
    serverName: string;
    /** 合服前区服（旧区服）名称，没有就设为 '1'，如果未合过服，则这里的值跟字段serverName的值一样，都为当前区服名称  */
    oldServerName: string;
    /** 角色id，没有就设为'1'  */
    roleId: string;
    /** 角色名称，没有就设为'1' */
    roleName: string;
    /** 角色等级 没有就写1  */
    level: number;
    /** vip等级 没有就写1  */
    viplevel: number;
    /** 玩家综合能力(如：战斗力)，没有就设为 1  */
    power: number;
    /** 玩家当前游戏币（如金币或元宝或钻石等）数量，没有就设为 0 */
    coinNumber: number;
    /** 1为成功创建角色时，2为登录完成后进入游戏后第一个界面那一刻，3为角色升级时，5为到达选服界面时，6为点击登陆按钮那一刻，7为玩家改名时 */
    action: number;
    /** 角色创建时间戳（精确到秒），类型int，没有就设为0 */
    createTime: number;
    /** 角色最后升级时间戳（精确到秒），类型int，没有就设为0 */
    lastUpgradeTime: number;
  }
  sdk.report(reportParams:ReportParams)
```