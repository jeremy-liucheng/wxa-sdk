# 前端接入准备

### 登陆

:::danger
需要在途悦 sdk 登陆后调用，需要用到途悦的 userId 和 openId
:::

- 地址：`https://wxlogin.${项目域名}/polymerize`

请求方式：POST

请求参数

| 字段名称 | 字段类型 | 是否必填 | 字段说明               |
| -------- | -------- | -------- | ---------------------- |
| cmd      | String   | 是       | 固定填写 tuYueLogin    |
| appid    | String   | 是       | 小游戏应用 id          |
| tyGameId | string   | 是       | 途悦 gameid            |
| openId   | String   | 是       | 途悦 登陆返回的 gameid |
| userId   | Number   | 是       | 途悦 登陆返回的 gameid |
| device   | Number   | 是       | 1 安卓，2ios           |
| gameid   | Number   | 是       | 平台游戏 id            |

返回值

```js
{
  uid: 30000000;
  token: "xxxxxxxx";
}
```

### 支付准备

:::danger
ios 需要投放之后 才能申请开白，目前没有 ios 支付；如果是 ios 端 要屏蔽充值的

部分渠道会有支付金额档位限制，请注意适配，如 B 站、微信小游戏订单金额，仅支持传入[ 1 , 3 , 6 , 8 , 12 , 18 , 25 ,30 , 40 , 45 , 50 , 60 , 68 , 73 ,78 , 88 , 98 , 108 , 118 , 128 ,148 , 168 , 188 , 198 , 328 , 648 ,998 , 1498 , 1998 , 2498 , 2998 ]
:::

商户后台配置商品，如下：

<img :src="$withBase('/images/wxa/shop.png')" alt="Image from dependency">

### 获取订单 id

:::danger
获取订单 id 之后调用
TYSDK.Pay(new TYPayParam()
{
orderId = "19988811",
}
)
:::

- 地址：https://login.${项目域名}/pay/tuyue/trans.php`

请求方式：GET

请求参数

| 字段名称      | 字段类型 | 是否必填 | 字段说明                                                                                                                                                                              |
| :------------ | :------- | :------- | :------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| product_id    | number   | 是       | 商品 id                                                                                                                                                                               |
| product_count | number   | 是       | 商商品数量                                                                                                                                                                            |
| tyUserId      | string   | 是       | 途悦 userid                                                                                                                                                                           |
| tyGameId      | string   | 是       | 途悦游戏 id                                                                                                                                                                           |
| token         | string   | 是       | 平台 token                                                                                                                                                                            |
| serverid      | number   | 是       | 区服 id                                                                                                                                                                               |
| uid           | number   | 是       | 爱微游平台用户 id                                                                                                                                                                     |
| gameid        | number   | 是       | 平台游戏 id                                                                                                                                                                           |
| txid          | string   | 否       | 商户订单号，最多 64 个字节。如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。                                            |
| userdata      | string   | 否       | 商户自定义参数，最多 128 个字节。如果不为空，则会在通知发货接口中返回给商户，请对该数据进行 url 转义，否则可能会丢失数据，可以参考 js 的 encodeURI 或者 php 中的 urlencode 进行转义。 |

返回值

```js
{
  tyOrderId: 19988811; // 支付途悦订单号 需要用这个订单号调用途悦支付接口
  transId: 222222222; // 支付平台订单号
}
```
