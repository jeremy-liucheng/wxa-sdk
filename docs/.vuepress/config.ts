import { defineUserConfig, defaultTheme } from "vuepress";
import { clipboardPlugin } from "vuepress-plugin-clipboard";
import { searchPlugin } from "@vuepress/plugin-search";

export default defineUserConfig({
  base: "/wxa-sdk/",
  lang: "zh-cn",
  title: "小游戏sdk归档",
  plugins: [
    searchPlugin({}),
    clipboardPlugin({
      successText: "复制成功",
      delay: 2000,
    }) as any,
  ],
  dest: "public",
  port: 9528,
  markdown: {
    headers: {
      level: [1, 2, 3, 4],
    },
  },
  head: [
    [
      "link",
      { rel: "icon", href: "https://cdn.11h5.com/static/image/logo_x.png" },
    ], // 需要被注入到当前页面的 HTML <head> 中的标签
    // 引入 Element UI 样式
  ],
  theme: defaultTheme({
    docsRepo: "http://gitlab.11h5.com/cody/vue-press",
    docsBranch: "master",
    docsDir: "docs",
    lastUpdated: true,
    editLinkText: "编辑此页",
    lastUpdatedText: "上次更新",
    contributors: true,
    contributorsText: "贡献人",
    sidebarDepth: 4,
    editLinkPattern: ":repo/-/edit/:branch/:path",
    navbar: [
      // NavbarGroup
      {
        text: "爱微游主包",
        children: [
          {
            text: "爱微游sdk",
            link: "/platform/wx/wxaMainFrontend.html",
          },
          {
            text: "小游戏中转页",
            link: "/platform/zhongzhuan/landPage.html",
          },
          {
            text: "抖音Unity接口",
            link: "/platform/byteUnity/byteUnityFrontend.html",
          },
        ],
      },
      {
        text: "其他渠道",
        children: [
          {
            text: "soul小游戏sdk",
            link: "/platform/soul/soulFrontend.html",
          },
          {
            text: "抖音小游戏主包sdk",
            link: "/platform/byte/byteMiniGameFrontend.html",
          },
          {
            text: "微信小游戏中手游渠道sdk",
            link: "/platform/zsy/zsyChannel.html",
          },
          {
            text: "途岳渠道unity对接",
            link: "/platform/unity/frontend.html",
          },
        ],
      },
      {
        text: "更新日志",
        children: [
          {
            text: "v2.5.4",
            link: "https://gitlab.11h5.com/docs/docbook/-/blob/master/platform/changelogs/wxa_main_changelog.md",
          },
        ],
      },
    ],
    sidebar: {
      "/platform/wx/": [
        {
          text: "微信小游戏主包sdk",
          children: ["wxaMainFrontend.md", "wxaMainBackend.md"],
        },
      ],
      "/platform/byte/": [
        {
          text: "抖音小游戏主包sdk",
          children: ["byteMiniGameFrontend.md", "byteMiniGameBackend.md"],
        },
      ],
      "/platform/zsy/": [
        {
          text: "中手游渠道主包sdk",
          children: ["zsyChannel.md"],
        },
      ],
      "/platform/soul/": [
        {
          text: "soul小游戏sdk",
          children: ["soulFrontend.md", "soulBackend.md"],
        },
      ],
      "/platform/cloudGame/": [
        {
          text: "云游戏",
          children: ["cloudGame.md"],
        },
      ],
      "/platform/zhongzhuan/": [
        {
          text: "中转页面需求",
          children: ["landPage.md"],
        },
      ],
      "/platform/unity/": [
        {
          text: "途悦需求",
          children: ["frontend.md", "backend.md"],
        },
      ],
      "/platform/byteUnity/": [
        {
          text: "抖音Unity需求",
          children: ["byteUnityFrontend.md", "byteUnityBackend.md"],
        },
      ],
    },
  }),
});
