const store = {}

console.log('version 0.1.2')

function SetData(args) {
  Object.assign(store, args);
}

function UrlReplace(url, domain = '11h5.com') {
  return url.replace(new RegExp('11h5.com', 'g'), domain);
}

function stat(type) {
  const {
    uid,
    chid,
    subchid
  } = GetData()
  wx.request({
    url: 'https://adstat.kxtoo.com/app?cmd=addAppWebLog',
    method: 'GET',
    data: {
      chid,
      subchid,
      uuid: uid,
      type
    },
  })
}

function GetData() {
  return store;
}

export default {
  userSource: async function (gameid) {
    return new Promise((resolve, reject) => {
      const {
        query: params
      } = wx.getLaunchOptionsSync()
      let {
        system
      } = wx.getSystemInfoSync();
      const device = system.includes("iOS") ? 2 : 1;
      const {
        miniProgram
      } = wx.getAccountInfoSync();
      const chidData = wx.getStorageSync('chidData')
      console.log(chidData, '====>cacheData')
      console.log(params, '====>launch params')
      if (!params.chid && !chidData.chid) {
        resolve({
          error: -1
        })
        return
      }

      wx.request({
        url: 'https://adstat.kxtoo.com/manager?cmd=app_getPageInfo',
        method: 'POST',
        data: {
          chid: params.chid || chidData.chid,
          subchid: params.subchid || chidData.subchid,
          gameid: params.gameid || chidData.gameid,
        },
        success(r) {
          if (r.data.error !== 0) {
            resolve({
              error: -1
            })
            return
          }
          if (Object.keys(r.data.data).length === 0) {
            resolve({
              error: -1
            })
            return
          }

          resolve({
            error: 0,
            ...r.data.data,
            headerImage: UrlReplace(r.data.data.header_image, 'kxtoo.com'),
            picImg: UrlReplace(r.data.data.pic_img, 'kxtoo.com'),
          })
        }
      })

      wx.login({
        complete: (res) => {
          const {
            code
          } = res;
          const args = {
            cmd: "wxaLogin",
            code,
            appid: miniProgram.appId,
            device,
            ...params,
            gameid,
            chid: params.chid || chidData.chid,
            subchid: params.subchid || chidData.subchid,
          };
          wx.request({
            url: "https://wxlogin.kxtoo.com/wxlogin",
            data: args,
            method: 'POST',
            success: (res) => {
              const data = {
                openid: res.data.openid,
                uid: res.data.uid,
                chid: params.chid || chidData.chid,
                subchid: params.subchid || chidData.subchid,
                gameid: params.gameid || chidData.gameid,
              }
              wx.setStorageSync('chidData', data)
              SetData(data)
              stat(1) //落地页打开
            },
          });
        },
      });
    })
  },
  copy: async function (type = 2) {
    const data = GetData()
    const query = Object.entries(data)
      .map(([key, value]) => `${key}=${value}`)
      .join("&");
    const url = `https://app.17bt.com?${query}&openid=${data.openid}&miniUid=${data.uid}`;
    stat(type) //落地页点击
    wx.setClipboardData({
      data: url,
      success() {
        wx.showToast({
          title: "复制成功",
          icon: "none",
        });
      },
      fail(r) {
        wx.showToast({
          title: `复制失败：${r.errno}`,
          icon: 'none'
        })
      }
    })
  },
}