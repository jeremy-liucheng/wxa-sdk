import{_ as n,o as s,c as a,a as e}from"./app-nYJy1wvu.js";const t={},p=e(`<h1 id="接入准备" tabindex="-1"><a class="header-anchor" href="#接入准备" aria-hidden="true">#</a> 接入准备</h1><div class="custom-container danger"><p class="custom-container-title">DANGER</p><p>sdk 调用时机（请严格遵守）：请在小游戏头包解压完，小游戏侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.</p></div><p>联系运营确定使用的域名，如运营提供的为<code>xxxx.com</code>，那么此项目 sdk 内用到的所有协议域名都是<code>xxxx.com</code>（下文中所有<code>xxxx.com</code>都是这个域名），下面将正式开始~</p><p>请将以下域名，加入到小游戏 <strong>request</strong> 白名单</p><blockquote><p><code>https://adapi.xxxx.com,https://act.xxxx.com,https://newintegral-wall.xxxx.com,https://adstat.xxxx.com,https://wxlogin.xxxx.com,https://platform.xxxx.com,https://api.xxxx.com,https://login.xxxx.com,https://zsy.coolthink.cn</code></p></blockquote><h3 id="下载sdk" tabindex="-1"><a class="header-anchor" href="#下载sdk" aria-hidden="true">#</a> <a href="/wxa-sdk/sdk/zsy/sdk_wxa.2.5.0.zip?v=1">下载sdk</a></h3><h3 id="始化sdk" tabindex="-1"><a class="header-anchor" href="#始化sdk" aria-hidden="true">#</a> 始化sdk</h3><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>  <span class="token keyword">import</span> Sdk <span class="token keyword">from</span> <span class="token string">&#39;本地sdk路径&#39;</span><span class="token punctuation">;</span>
  <span class="token keyword">const</span> sdk <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Sdk</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    <span class="token literal-property property">appid</span><span class="token operator">:</span><span class="token string">&#39;xxxxxxx&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 微信后台appid</span>
    <span class="token literal-property property">gameid</span><span class="token operator">:</span><span class="token string">&#39;xxxxxxx&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 商户后台gameid</span>
    <span class="token literal-property property">zsyGame</span><span class="token operator">:</span><span class="token string">&#39;xxxxxxx&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 中手游游戏标识别 联系运营提供</span>
    <span class="token literal-property property">zsyChannel</span><span class="token operator">:</span><span class="token string">&#39;xxxxxxx&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 中手游渠道标识 联系运营提供</span>
    <span class="token literal-property property">zsySubChannel</span><span class="token operator">:</span><span class="token string">&#39;xxxxxxx&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 中手游子渠道标识 联系运营提供</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="登陆" tabindex="-1"><a class="header-anchor" href="#登陆" aria-hidden="true">#</a> 登陆</h3><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>  sdk<span class="token punctuation">.</span><span class="token function">login</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter"><span class="token punctuation">{</span>uid<span class="token punctuation">,</span>token<span class="token punctuation">,</span>openid<span class="token punctuation">}</span></span><span class="token punctuation">)</span><span class="token operator">=&gt;</span><span class="token punctuation">{</span><span class="token punctuation">}</span><span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="支付准备" tabindex="-1"><a class="header-anchor" href="#支付准备" aria-hidden="true">#</a> 支付准备</h3><p><strong class="red">部分渠道会有支付金额档位限制，请注意适配，如B站、微信小游戏订单金额，仅支持传入[ 1 , 3 , 6 , 8 , 12 , 18 , 25 ,30 , 40 , 45 , 50 , 60 , 68 , 73 ,78 , 88 , 98 , 108 , 118 , 128 ,148 , 168 , 188 , 198 , 328 , 648 ,998 , 1498 , 1998 , 2498 , 2998 ]</strong></p><h3 id="支付" tabindex="-1"><a class="header-anchor" href="#支付" aria-hidden="true">#</a> 支付</h3><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code>  <span class="token keyword">interface</span> <span class="token class-name">PayParams</span> <span class="token punctuation">{</span>
    <span class="token doc-comment comment">/** 合服后的区服（玩家当前所在区服）ID */</span>
    serverId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服后的区服（玩家当前所在区服）名称 */</span>
    serverName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服前的区服 */</span>
    oldServerId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服前的区服（旧区服）名称 */</span>
    oldServerName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色id */</span>
    roleId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 玩家名称 */</span>
    roleName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 玩家当前游戏币（如金币或元宝或钻石等）数量 */</span>
    coinNumber<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 改名前角色名称，如果未改过名，则这里的值跟字段roleName的值一样，都为当前角色名称 */</span>
    oldRoleName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 充值主题,字数不超过200个字符,例如:购买哪个等级或购买什么道具 */</span>
    subject<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 商品id */</span>
    productId<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色等级 */</span>
    roleLevel<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色vip等级 如果没有填0 */</span>
    roleVipLevel<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 游戏扩展字段,可为空值,长度不超过250个字符，该参数会在支付回调时原样返回 */</span>
    gameExtend<span class="token operator">?</span><span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
  sdk<span class="token punctuation">.</span><span class="token function">pay</span><span class="token punctuation">(</span>payParams<span class="token operator">:</span>PayParams<span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="上报" tabindex="-1"><a class="header-anchor" href="#上报" aria-hidden="true">#</a> 上报</h3><div class="language-typescript line-numbers-mode" data-ext="ts"><pre class="language-typescript"><code>  <span class="token keyword">interface</span> <span class="token class-name">ReportParams</span> <span class="token punctuation">{</span>
    <span class="token doc-comment comment">/** 游戏类型（比如：武侠、休闲、竞技、枪战 等等)  */</span>
    gameType<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服后的区服（玩家当前所在区服）ID，没有就设为 &#39;1&#39;  */</span>
    serverId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服前的区服（旧区服）ID，没有则默认为&#39;1&#39;，如果未合过服，则这里的值跟字段serverId的值一样，都为当前区服ID  */</span>
    oldServerId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服后区服（玩家当前所在区服）名称，没有就设为 &#39;1&#39;  */</span>
    serverName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 合服前区服（旧区服）名称，没有就设为 &#39;1&#39;，如果未合过服，则这里的值跟字段serverName的值一样，都为当前区服名称  */</span>
    oldServerName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色id，没有就设为&#39;1&#39;  */</span>
    roleId<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色名称，没有就设为&#39;1&#39; */</span>
    roleName<span class="token operator">:</span> <span class="token builtin">string</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色等级 没有就写1  */</span>
    level<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** vip等级 没有就写1  */</span>
    viplevel<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 玩家综合能力(如：战斗力)，没有就设为 1  */</span>
    power<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 玩家当前游戏币（如金币或元宝或钻石等）数量，没有就设为 0 */</span>
    coinNumber<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 1为成功创建角色时，2为登录完成后进入游戏后第一个界面那一刻，3为角色升级时，5为到达选服界面时，6为点击登陆按钮那一刻，7为玩家改名时 */</span>
    action<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色创建时间戳（精确到秒），类型int，没有就设为0 */</span>
    createTime<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 角色最后升级时间戳（精确到秒），类型int，没有就设为0 */</span>
    lastUpgradeTime<span class="token operator">:</span> <span class="token builtin">number</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
  sdk<span class="token punctuation">.</span><span class="token function">report</span><span class="token punctuation">(</span>reportParams<span class="token operator">:</span>ReportParams<span class="token punctuation">)</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div>`,16),o=[p];function c(i,l){return s(),a("div",null,o)}const d=n(t,[["render",c],["__file","zsyChannel.html.vue"]]);export{d as default};
