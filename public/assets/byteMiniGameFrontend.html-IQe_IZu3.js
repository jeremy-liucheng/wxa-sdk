import{_ as p,r as c,o,c as i,b as n,d as s,e as l,a as e}from"./app-nYJy1wvu.js";const r={},d=e(`<h1 id="前端接入准备" tabindex="-1"><a class="header-anchor" href="#前端接入准备" aria-hidden="true">#</a> 前端接入准备</h1><p><strong class="red">sdk 调用时机（请严格遵守）：请在小游戏头包解压完，小游戏侧做出首屏渲染之前即刻调用 SDK 并初始化，即所有游戏侧执行代码的最前部.</strong></p><p>联系运营确定使用的域名，如运营提供的为<code>xxxx.com</code>，那么此项目 sdk 内用到的所有协议域名都是<code>xxxx.com</code>（下文中所有<code>xxxx.com</code>都是这个域名），下面将正式开始~</p><p>请将以下域名，加入到小游戏 <strong>request</strong> 白名单</p><blockquote><p><code>https://adapi.xxxx.com,https://act.xxxx.com,https://newintegral-wall.xxxx.com,https://adstat.xxxx.com,https://wxlogin.xxxx.com,https://platform.xxxx.com,https://api.xxxx.com,https://login.xxxx.com</code></p></blockquote><p>请将以下域名，加入到小游戏 <strong>downloadFile</strong> 白名单</p><blockquote><p><code>https://api.xxxx.com,https://act.xxxx.com</code></p></blockquote><h3 id="下载-sdk" tabindex="-1"><a class="header-anchor" href="#下载-sdk" aria-hidden="true">#</a> <a href="/wxa-sdk/sdk/byte/byte_wxa.0.0.1.zip">下载 sdk</a></h3><h2 id="必接" tabindex="-1"><a class="header-anchor" href="#必接" aria-hidden="true">#</a> 必接</h2><h3 id="初始化-sdk" tabindex="-1"><a class="header-anchor" href="#初始化-sdk" aria-hidden="true">#</a> 初始化 sdk</h3><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>
<span class="token keyword">import</span> Sdk <span class="token keyword">from</span> <span class="token string">&#39;本地sdk路径&#39;</span><span class="token punctuation">;</span>

<span class="token keyword">const</span> sdk <span class="token operator">=</span> <span class="token keyword">new</span> <span class="token class-name">Sdk</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    <span class="token comment">// 必填字段，传入当前游戏ID</span>
    <span class="token literal-property property">gameid</span><span class="token operator">:</span> gameid<span class="token punctuation">,</span>
    <span class="token comment">// 必填字段，当前小游戏的appid</span>
    <span class="token literal-property property">appid</span><span class="token operator">:</span> appid<span class="token punctuation">,</span>
    <span class="token comment">// 选填字段，联系运营提供</span>
    domain<span class="token operator">?</span><span class="token operator">:</span> <span class="token string">&#39;xxx.com&#39;</span><span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="登录" tabindex="-1"><a class="header-anchor" href="#登录" aria-hidden="true">#</a> 登录</h3><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token comment">// uid默认为0，token为空 （token有效期为12个小时）</span>
<span class="token comment">// 如果登录成功，将可获得用户的uid，token信息</span>
<span class="token comment">// token会缓存在storge里面 下次登陆验证token有没有过期 （注意：删除uid token如果还在有效期会返回老的uid）</span>
<span class="token comment">// 默认为h5互通版本</span>
sdk
  <span class="token punctuation">.</span><span class="token function">login</span><span class="token punctuation">(</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter"><span class="token punctuation">{</span> uid<span class="token punctuation">,</span> token <span class="token punctuation">}</span></span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// 登录成功</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// 登录失败</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="更新用户信息" tabindex="-1"><a class="header-anchor" href="#更新用户信息" aria-hidden="true">#</a> 更新用户信息</h3><p>静默授权，无法获取用户的头像、昵称等敏感信息</p><p>如果项目需要获取用户的个人信息，需要调用此方法，进行信息同步</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>
<span class="token keyword">const</span> userInfo <span class="token operator">=</span> <span class="token keyword">await</span> sdk<span class="token punctuation">.</span><span class="token function">updateUserInfo</span><span class="token punctuation">(</span><span class="token punctuation">)</span>

UserInfo <span class="token punctuation">{</span>
    <span class="token doc-comment comment">/** 用户昵称 */</span>
    <span class="token literal-property property">nickName</span><span class="token operator">:</span> string<span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户头像图片的 URL。
     * URL 最后一个数值代表正方形头像大小（有 0、46、64、96、132 数值可选，
     * 0 代表 640x640 的正方形头像，46 表示 46x46 的正方形头像，剩余数值以此类推。默认132），
     * 用户没有头像时该项为空。若用户更换头像，原有头像 URL 将失效。 */</span>
    <span class="token literal-property property">avatarUrl</span><span class="token operator">:</span> string<span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户性别 */</span>
    <span class="token literal-property property">gender</span><span class="token operator">:</span> <span class="token number">0</span> <span class="token operator">|</span> <span class="token number">1</span> <span class="token operator">|</span> <span class="token number">2</span><span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户所在国家 */</span>
    <span class="token literal-property property">country</span><span class="token operator">:</span> string<span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户所在省份 */</span>
    <span class="token literal-property property">province</span><span class="token operator">:</span> string<span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户所在城市 */</span>
    <span class="token literal-property property">city</span><span class="token operator">:</span> string<span class="token punctuation">;</span>
    <span class="token doc-comment comment">/** 用户语言，目前为空 */</span>
    <span class="token literal-property property">language</span><span class="token operator">:</span> <span class="token string">&#39;&#39;</span>
<span class="token punctuation">}</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="联系客服" tabindex="-1"><a class="header-anchor" href="#联系客服" aria-hidden="true">#</a> 联系客服</h3><p><strong class="red">此功能在游戏上线前必须接入，客服入口 icon 要放在显眼位置！！</strong></p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>sdk<span class="token punctuation">.</span><span class="token function">openCustomerServiceConversation</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  <span class="token literal-property property">image</span><span class="token operator">:</span> string<span class="token punctuation">,</span> <span class="token comment">// 图标url</span>
  <span class="token literal-property property">style</span><span class="token operator">:</span> <span class="token punctuation">{</span>
    <span class="token literal-property property">left</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 横坐标</span>
    <span class="token literal-property property">top</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 纵坐标</span>
    <span class="token literal-property property">width</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 图标宽度</span>
    <span class="token literal-property property">height</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 图标高度</span>
    borderRadius<span class="token operator">?</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 边框圆角 （可选 默认 4）</span>
    borderColor<span class="token operator">?</span><span class="token operator">:</span> <span class="token string">&#39;#ffffff&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 边框颜色 （可选: 默认 &#39;#ffffff&#39;）</span>
    backgroundColor<span class="token operator">?</span><span class="token operator">:</span> <span class="token string">&#39;#ffffff&#39;</span><span class="token punctuation">,</span> <span class="token comment">// 边框颜色 （可选 默认 &#39;#ffffff&#39;）</span>
    borderWidth<span class="token operator">?</span><span class="token operator">:</span> number<span class="token punctuation">,</span> <span class="token comment">// 边框宽度 （可选 默认 1）</span>
  <span class="token punctuation">}</span><span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token parameter">res</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>res<span class="token punctuation">,</span> <span class="token string">&#39;====&gt;创建成功&#39;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token parameter">err</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    console<span class="token punctuation">.</span><span class="token function">log</span><span class="token punctuation">(</span>err<span class="token punctuation">,</span> <span class="token string">&#39;====&gt;创建失败&#39;</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="有内购功能游戏必接入" tabindex="-1"><a class="header-anchor" href="#有内购功能游戏必接入" aria-hidden="true">#</a> 有内购功能游戏必接入</h2><h3 id="苹果支付" tabindex="-1"><a class="header-anchor" href="#苹果支付" aria-hidden="true">#</a> 苹果支付</h3><h4 id="支付准备" tabindex="-1"><a class="header-anchor" href="#支付准备" aria-hidden="true">#</a> 支付准备</h4><p><strong class="red">此功能在游戏上线前必须接入</strong></p><p>商户后台配置商品，如下：</p>`,25),u=["src"],m=e(`<h4 id="支付" tabindex="-1"><a class="header-anchor" href="#支付" aria-hidden="true">#</a> 支付</h4><p>使用前请前往 https://microapp.bytedance.com/game/tt05f973449ac0036302/traffic-other/customer 绑定抖音 IM 客服后续充值相关问题用户都会通过该客服进行咨询（请勿用绑定的抖音号进行支付测试，会导致无法拉起客服界面）（本步骤未完成报错：fail im disable ）</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token comment">// 第一步：点击充值按钮获取订单信息</span>
  <span class="token keyword">const</span> <span class="token punctuation">[</span>ret<span class="token punctuation">,</span> err<span class="token punctuation">]</span> <span class="token operator">=</span> <span class="token keyword">await</span> sdk<span class="token punctuation">.</span><span class="token function">getOrderInfo</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      <span class="token literal-property property">product_id</span><span class="token operator">:</span> number<span class="token punctuation">,</span>
      <span class="token literal-property property">product_count</span><span class="token operator">:</span> number<span class="token punctuation">,</span>
      <span class="token doc-comment comment">/** 当前的区服  */</span>
      <span class="token literal-property property">serverid</span><span class="token operator">:</span> <span class="token string">&#39;1&#39;</span><span class="token punctuation">,</span>
      <span class="token doc-comment comment">/**
       * 商户订单号，最多64个字节。
       * 如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。
       */</span>
      txid<span class="token operator">?</span><span class="token operator">:</span> string<span class="token punctuation">,</span>
      <span class="token doc-comment comment">/** 商户自定义参数，最多128个字节。
       * 如果不为空，则会在通知发货接口中返回给商户，请对该数据进行url转义，否则可能会丢失数据，
       * 可以参考js的encodeURI或者php中的urlencode进行转义。 */</span>
      userdata<span class="token operator">?</span><span class="token operator">:</span> string<span class="token punctuation">,</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token doc-comment comment">/**
   * ret成功返回 <span class="token punctuation">{</span>&quot;error&quot;:0,&quot;errmsg&quot;:&quot;操作成功&quot;,&quot;trans_id&quot;:number,&quot;buy_quantity&quot;:100<span class="token punctuation">}</span>
   * 失败返回 null
   */</span>

  <span class="token doc-comment comment">/**
   * 第二步：由于抖音ios支付需要原生点击事件 订单信息获取成功后需要cp自行绘制二次确认按钮 在二次确认按钮的原生点击事件回调中调用 (抖音ios支付限制必须是点击回调，所以不是promise风格 不支持.then调用)
   */</span>
  sdk<span class="token punctuation">.</span><span class="token function">iosPay</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      <span class="token literal-property property">buy_quantity</span><span class="token operator">:</span> ret<span class="token punctuation">.</span>buy_quantity<span class="token punctuation">,</span> <span class="token comment">// 上一步订单信息中的buy_quantity</span>
      <span class="token literal-property property">trans_id</span><span class="token operator">:</span> <span class="token function">String</span><span class="token punctuation">(</span>ret<span class="token punctuation">.</span>trans_id<span class="token punctuation">)</span><span class="token punctuation">,</span> <span class="token comment">// 上一步订单信息中的trans_id</span>
      <span class="token literal-property property">callBack</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token parameter">res</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span> <span class="token comment">// 回调函数</span>
        <span class="token comment">// res成功回调 { error: 0, errmsg: &#39;操作成功&#39;, trans_id }</span>
        <span class="token comment">// res失败回调 {errmsg:string, error:number}</span>
      <span class="token punctuation">}</span><span class="token punctuation">)</span>
  <span class="token punctuation">}</span><span class="token punctuation">)</span>
<span class="token comment">// 第三步: 抖音弹出进入客服提示 用户点击进入客服支付</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><ul><li><p>Tip: 用户成功拉起客服界面后即回调成功，未成功拉起客服界面即回调失败，与实际支付结果无关，是否支付成功请通过服务端接口查询余额自行保证。</p></li><li><p>Tip：如果遇到 fail must invoke by user gesture 的报错信息，是因为该接口需要由点击行为触发，请确保在按钮的点击 回调中 使用本接口。</p><ul><li>tt.createInteractiveButton 接口创建的 native 按钮，无法通过本限制。</li><li>该限制预期在抖音 23.0.0 版本去除，这期间如果需要通过网络请求获取订单号无法避开这个限制的话，可以做成二次确认的形式，第一次点击通过请求获取订单号，第二次点击调用本接口。</li></ul></li></ul><h3 id="安卓支付" tabindex="-1"><a class="header-anchor" href="#安卓支付" aria-hidden="true">#</a> 安卓支付</h3><h4 id="安卓支付准备" tabindex="-1"><a class="header-anchor" href="#安卓支付准备" aria-hidden="true">#</a> 安卓支付准备</h4><p><strong class="red">此功能在游戏上线前必须接入</strong></p><ul><li>申请小游戏虚拟支付（申请抖音支付前需要绑定商户，绑定操作需要找对应运营进行绑定。）</li></ul><h4 id="配置人民币兑换游戏币比例" tabindex="-1"><a class="header-anchor" href="#配置人民币兑换游戏币比例" aria-hidden="true">#</a> 配置人民币兑换游戏币比例</h4>`,9),k=n("li",null,"在商户平台游戏配置中将兑换比例配置成 1:100。",-1),v={href:"https://microapp.bytedance.com/",target:"_blank",rel:"noopener noreferrer"},b=n("h4",{id:"支付配置",tabindex:"-1"},[n("a",{class:"header-anchor",href:"#支付配置","aria-hidden":"true"},"#"),s(" 支付配置")],-1),h=["src"],g=n("p",null,"小游戏后台填写支付回调地址：'https://login.{业务域名}.com/pay/bytedance/notify.php'",-1),x=n("p",null,"商户后台配置，如下：",-1),f=["src"],y=e(`<p>appid、签名秘钥、服务器回调 Token 这三个参数需要平台这边管理员添加到商户后台游戏私有参数</p><h4 id="支付开关" tabindex="-1"><a class="header-anchor" href="#支付开关" aria-hidden="true">#</a> 支付开关</h4><p>在商户平台，在游戏的额外参数中配置开关，如果不配置默认是开启支付。</p><table><thead><tr><th style="text-align:left;">参数名称</th><th style="text-align:left;">参数描述</th></tr></thead><tbody><tr><td style="text-align:left;">closePay</td><td style="text-align:left;">支付开关<br>0 或者不配置表示打开支付<br>1 表示关闭支付</td></tr></tbody></table><h4 id="支付-1" tabindex="-1"><a class="header-anchor" href="#支付-1" aria-hidden="true">#</a> 支付</h4><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>
sdk<span class="token punctuation">.</span><span class="token function">pay</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
    <span class="token doc-comment comment">/** 商户后台配置商品后，生成的商品ID */</span>
    <span class="token literal-property property">product_id</span><span class="token operator">:</span> <span class="token string">&#39;商品ID&#39;</span><span class="token punctuation">,</span>
    <span class="token doc-comment comment">/**
     * 代表购买商品的数量，默认值为1。
     * 批量购买商品时使用，最大不要超过1000。
     * 如果不为空，则会在通知发货接口中返回此参数。
     */</span>
    <span class="token literal-property property">product_count</span><span class="token operator">:</span> number<span class="token punctuation">,</span>
    <span class="token doc-comment comment">/** 当前的区服  */</span>
    <span class="token literal-property property">serverid</span><span class="token operator">:</span> <span class="token string">&#39;1&#39;</span><span class="token punctuation">,</span>
    <span class="token doc-comment comment">/**
     * 商户订单号，最多64个字节。
     * 如果不为空，则支付平台会检测商户订单号是否重复，防止用户在同一个页面支付两次，并且会在通知发货接口中原样返回。
     */</span>
    txid<span class="token operator">?</span><span class="token operator">:</span> string<span class="token punctuation">,</span>
    <span class="token doc-comment comment">/** 商户自定义参数，最多128个字节。
     * 如果不为空，则会在通知发货接口中返回给商户，请对该数据进行url转义，否则可能会丢失数据，
     * 可以参考js的encodeURI或者php中的urlencode进行转义。 */</span>
    userdata<span class="token operator">?</span><span class="token operator">:</span> string<span class="token punctuation">,</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">then</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// 支付成功</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">.</span><span class="token function">catch</span><span class="token punctuation">(</span><span class="token punctuation">(</span><span class="token parameter">res</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
    <span class="token comment">// 支付失败</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span>

<span class="token comment">// 支付错误码，将会通过throw 抛出</span>
<span class="token punctuation">{</span>
    <span class="token literal-property property">error</span><span class="token operator">:</span>  <span class="token number">0</span> <span class="token comment">//操作成功</span>
            <span class="token number">1</span> <span class="token comment">//缺少参数</span>
            <span class="token number">2</span> <span class="token comment">//系统错误</span>
            <span class="token number">3</span> <span class="token comment">//token失效,请尝试重新登录</span>
            <span class="token number">4</span> <span class="token comment">//该appid未配置支付参数</span>
            <span class="token number">5</span> <span class="token comment">//商品不存在</span>
            <span class="token number">6</span> <span class="token comment">//游戏不存在</span>
            <span class="token number">7</span> <span class="token comment">//系统繁忙，请稍后再尝试购买</span>
            <span class="token number">8</span> <span class="token comment">//游戏币余额不足</span>
            <span class="token number">9</span> <span class="token comment">//缺少参数token</span>
            <span class="token number">10</span> <span class="token comment">//缺少参数appid</span>
            <span class="token number">11</span> <span class="token comment">//缺少参数product_id</span>
            <span class="token number">12</span> <span class="token comment">//购买数量错误，必须在1-1000之间</span>
            <span class="token number">13</span> <span class="token comment">//虚拟支付还在接入中，请使用沙箱环境测试</span>
            <span class="token number">14</span> <span class="token comment">//缺少参数订单号</span>
            <span class="token number">15</span> <span class="token comment">//订单不存在</span>
            <span class="token number">16</span> <span class="token comment">//订单已处理</span>
            <span class="token number">17</span> <span class="token comment">//支付用户和下单用户不一致</span>
            <span class="token number">18</span> <span class="token comment">//订单正在处理中，将在稍后到账</span>
            <span class="token number">19</span> <span class="token comment">//缺少参数gameid</span>
            <span class="token number">20</span> <span class="token comment">//游戏币兑换比例未配置，请在商品平台上进行配置</span>
<span class="token punctuation">}</span>

</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h2 id="游戏内有分享功能必接入" tabindex="-1"><a class="header-anchor" href="#游戏内有分享功能必接入" aria-hidden="true">#</a> 游戏内有分享功能必接入</h2><h3 id="有效创角上报" tabindex="-1"><a class="header-anchor" href="#有效创角上报" aria-hidden="true">#</a> 有效创角上报</h3><p>双方约定当用户达到有效创角的条件，调用下面的方法进行上报</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>sdk<span class="token punctuation">.</span><span class="token function">effectiveStat</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="获取分享参数" tabindex="-1"><a class="header-anchor" href="#获取分享参数" aria-hidden="true">#</a> 获取分享参数</h3><p>获取一个参数对象，该对象里面的参数均需要追加到分享的 path 路径上</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">const</span> query <span class="token operator">=</span> sdk<span class="token punctuation">.</span><span class="token function">getShareParams</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">/*
    返回: 
    { 
      chid:number
      fuid:number
      subchid:string
      suid:number
      ...一些游戏自定义的参数 
    }
    如果小游戏测分享需要带自己游戏自定义的参数 上述参数只需拼接chid fuid subchid suid 
    需要按抖音官方分享接口拼接相关参数
*/</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="分享上报" tabindex="-1"><a class="header-anchor" href="#分享上报" aria-hidden="true">#</a> 分享上报</h3><p>玩家分享完成，调用此方法 （按照运营规定是否接入）</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>sdk<span class="token punctuation">.</span><span class="token function">mpShareStat</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h3 id="获取启动参数" tabindex="-1"><a class="header-anchor" href="#获取启动参数" aria-hidden="true">#</a> 获取启动参数</h3><p>获取解析后的启动参数</p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">const</span> query <span class="token operator">=</span> sdk<span class="token punctuation">.</span><span class="token function">getQuery</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div><h2 id="选接" tabindex="-1"><a class="header-anchor" href="#选接" aria-hidden="true">#</a> 选接</h2><h3 id="判断前置游戏是否开启" tabindex="-1"><a class="header-anchor" href="#判断前置游戏是否开启" aria-hidden="true">#</a> 判断前置游戏是否开启</h3><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code><span class="token keyword">const</span> isOpen <span class="token operator">=</span> <span class="token keyword">await</span> sdk<span class="token punctuation">.</span><span class="token function">isOpenPreGame</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token comment">// isOpen: true 开启 ，false 关闭</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div><div class="line-number"></div></div></div><h3 id="用户完成前置小游戏" tabindex="-1"><a class="header-anchor" href="#用户完成前置小游戏" aria-hidden="true">#</a> 用户完成前置小游戏</h3><p><strong class="red">仅在前置游戏开启情况下调用</strong></p><div class="language-javascript line-numbers-mode" data-ext="js"><pre class="language-javascript"><code>sdk<span class="token punctuation">.</span><span class="token function">achievePreGame</span><span class="token punctuation">(</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
</code></pre><div class="line-numbers" aria-hidden="true"><div class="line-number"></div></div></div>`,25);function _(a,j){const t=c("ExternalLinkIcon");return o(),i("div",null,[d,n("img",{src:a.$withBase("/images/byte/shop.png"),alt:"Image from dependency"},null,8,u),m,n("ul",null,[k,n("li",null,[s("在"),n("a",v,[s("抖音开发者后台"),l(t)]),s("，修改虚拟支付游戏币兑换比例为 1:100，即 1 个游戏币=1 分钱人民币。")])]),b,n("p",null,[s("字节后台配置，如下： "),n("img",{src:a.$withBase("/images/byte/byte_backend.png"),alt:"Image from dependency"},null,8,h)]),g,x,n("img",{src:a.$withBase("/images/byte/commercial_backend_pay.png"),alt:"Image from dependency"},null,8,f),y])}const q=p(r,[["render",_],["__file","byteMiniGameFrontend.html.vue"]]);export{q as default};
